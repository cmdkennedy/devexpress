using System;
using DevExpress.XtraScheduler.Reporting;
using DevExpress.XtraScheduler.Services;
using DevExpress.XtraScheduler.Demos.Reporting.DateFormatting;

namespace DevExpress.XtraScheduler.Demos.Reporting.VisibleWeekDays {

    public class PreviewControl : ReportPreviewControlBase {
        private System.ComponentModel.IContainer components = null;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraScheduler.UI.WeekDaysCheckEdit weekDaysCheckEdit1;
        WeekDays visibleWeekDays = WeekDays.EveryDay;
        private MonthReport monthReport;
        private DevExpress.XtraEditors.RadioGroup rgrpView;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DayReport dayReport;

		public PreviewControl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			InitializeControlValues();            
		}
        SchedulerViewType ViewType { get { return (SchedulerViewType)rgrpView.EditValue; } }
        MonthReport MonthReport { get { return monthReport; } }
        DayReport DayReport { get { return dayReport; } }
        WeekDays VisibleWeekDays { get { return visibleWeekDays; } set { visibleWeekDays = value; } }
        
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
        protected override void CreateReports() {
            if (monthReport != null)
                monthReport.Dispose();
            this.monthReport = new MonthReport();
            if (dayReport != null)
                dayReport.Dispose();
            this.dayReport = new DayReport();
        }
        protected override XtraSchedulerReport GetActiveReport() {
            if (ViewType == SchedulerViewType.Month)
                return MonthReport;
            else
                return DayReport;
        }
        protected override void InitAdapterTimeInterval() {
            StoragePrintAdapter.TimeInterval = new TimeInterval(DemoUtils.Date, TimeSpan.FromDays(7 * 8));
        }
		void InitializeControlValues() {
            IVisibleWeekDaysReport report = (IVisibleWeekDaysReport)GetActiveReport();
            this.VisibleWeekDays = report.VisibleWeekDays;

		}
		protected override void UpdateReportProperties(XtraSchedulerReport report) {
			base.UpdateReportProperties(report);
            if (ViewType == SchedulerViewType.Month)
                ((MonthReport)report).VisibleWeekDays = this.VisibleWeekDays;
            else {
                DayReport dayReport = (DayReport)report;
                dayReport.VisibleWeekDays = this.VisibleWeekDays;
                dayReport.DayCount = CalculateDayCount();
            }
            UpdateFormatServices();
		}
        private int CalculateDayCount() {            
            int dayCount = DevExpress.XtraScheduler.Native.DateTimeHelper.ToDayOfWeeks(this.VisibleWeekDays).Length;
            return dayCount == 0 ? 7 : dayCount;
        }
		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.weekDaysCheckEdit1 = new DevExpress.XtraScheduler.UI.WeekDaysCheckEdit();
            this.rgrpView = new DevExpress.XtraEditors.RadioGroup();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            this.pnlSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weekDaysCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgrpView.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.groupControl1);
            this.pnlSettings.Controls.Add(this.groupControl2);
            this.pnlSettings.Size = new System.Drawing.Size(720, 94);            
            this.pnlSettings.Controls.SetChildIndex(this.groupControl2, 0);
            this.pnlSettings.Controls.SetChildIndex(this.btnEdit, 0);
            this.pnlSettings.Controls.SetChildIndex(this.groupControl1, 0);
            // 
            // panelSeparatorControl
            // 
            this.panelSeparatorControl.Location = new System.Drawing.Point(0, 94);
            this.panelSeparatorControl.Size = new System.Drawing.Size(720, 10);
            // 
            // printControl
            // 
            this.printControl.Location = new System.Drawing.Point(0, 104);
            this.printControl.Size = new System.Drawing.Size(720, 337);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(640, 64);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.weekDaysCheckEdit1);
            this.groupControl2.Location = new System.Drawing.Point(99, 8);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(0);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(534, 79);
            this.groupControl2.TabIndex = 79;
            this.groupControl2.Text = "Visible Weekdays";
            // 
            // weekDaysCheckEdit1
            // 
            this.weekDaysCheckEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.weekDaysCheckEdit1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.weekDaysCheckEdit1.Appearance.Options.UseBackColor = true;
            this.weekDaysCheckEdit1.Location = new System.Drawing.Point(11, 38);
            this.weekDaysCheckEdit1.Margin = new System.Windows.Forms.Padding(0);
            this.weekDaysCheckEdit1.Name = "weekDaysCheckEdit1";
            this.weekDaysCheckEdit1.Size = new System.Drawing.Size(501, 41);
            this.weekDaysCheckEdit1.TabIndex = 6;
            this.weekDaysCheckEdit1.WeekDaysChanged += new System.EventHandler(this.weekDaysCheckEdit1_WeekDaysChanged_1);
            // 
            // rgrpView
            // 
            this.rgrpView.EditValue = DevExpress.XtraScheduler.SchedulerViewType.Month;
            this.rgrpView.Location = new System.Drawing.Point(10, 20);
            this.rgrpView.Name = "rgrpView";
            this.rgrpView.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rgrpView.Properties.Appearance.Options.UseBackColor = true;
            this.rgrpView.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rgrpView.Properties.Columns = 1;
            this.rgrpView.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(DevExpress.XtraScheduler.SchedulerViewType.Day, "Day"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(DevExpress.XtraScheduler.SchedulerViewType.Month, "Month")});
            this.rgrpView.Size = new System.Drawing.Size(66, 59);
            this.rgrpView.TabIndex = 81;
            this.rgrpView.SelectedIndexChanged += new System.EventHandler(this.rgrpView_SelectedIndexChanged_1);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.rgrpView);
            this.groupControl1.Location = new System.Drawing.Point(7, 8);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(87, 79);
            this.groupControl1.TabIndex = 83;
            this.groupControl1.Text = "Report Type";
            // 
            // PreviewControl
            // 
            this.Name = "PreviewControl";
            this.Size = new System.Drawing.Size(720, 441);
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            this.pnlSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.weekDaysCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgrpView.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion
             
        private void weekDaysCheckEdit1_WeekDaysChanged_1(object sender, EventArgs e) {
            this.VisibleWeekDays = weekDaysCheckEdit1.WeekDays;
            UpdateActiveReport();
        }        

        private void rgrpView_SelectedIndexChanged_1(object sender, EventArgs e) {
            this.VisibleWeekDays = weekDaysCheckEdit1.WeekDays;
            UpdateActiveReport();
        }
        
        public void UpdateFormatServices() {
            StoragePrintAdapter.RemoveService(typeof(IHeaderCaptionService));
            IHeaderCaptionService customHeaderCaptionService = new CustomHeaderCaptionService("MM/dd, ddd");
            StoragePrintAdapter.AddService(typeof(IHeaderCaptionService), customHeaderCaptionService);                       
        }
	}
    public interface IVisibleWeekDaysReport {
        WeekDays VisibleWeekDays { get; set;}        
    }
}

