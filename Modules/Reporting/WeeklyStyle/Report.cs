using System;
using DevExpress.XtraScheduler.Reporting;

namespace DevExpress.XtraScheduler.Demos.Reporting.WeeklyStyle
{
	/// <summary>
	/// Summary description for Report.
	/// </summary>
	public partial class Report : XtraSchedulerReport {

		public Report() {

			InitializeComponent();
		}

        public int ResourceCount { get { return reportWeekView1.VisibleResourceCount; } set {	reportWeekView1.VisibleResourceCount = value; } }

        public int ColumnCount {
            get { return reportWeekView1.VisibleWeekDayColumnCount; }
            set {
                if (reportWeekView1.VisibleWeekDayColumnCount == value)
                    return;
                reportWeekView1.VisibleWeekDayColumnCount = value;
                OnColumnCountChanged();
            }
        }
        private void OnColumnCountChanged() {
            if (ColumnCount == 2) {
                this.timeIntervalInfo1.PrintInColumn = PrintInColumnMode.Odd;
                this.calendarControl1.PrintInColumn = PrintInColumnMode.Even;
                this.timeIntervalInfo1.PrintContentMode = PrintContentMode.AllColumns;
            } else {
                this.timeIntervalInfo1.PrintInColumn = PrintInColumnMode.All;
                this.calendarControl1.PrintInColumn = PrintInColumnMode.All;
                this.timeIntervalInfo1.PrintContentMode = PrintContentMode.CurrentColumn;
            }
        }

	}
}
 
