using System;
using DevExpress.XtraScheduler.Reporting;

namespace DevExpress.XtraScheduler.Demos.Reporting.WeeklyStyle {

	public class PreviewControl : SingleReportPreviewControl {
		private System.ComponentModel.IContainer components = null;
		private DevExpress.XtraEditors.LabelControl lbLayout;
		private DevExpress.XtraEditors.ComboBoxEdit cmbLayout;
        private DevExpress.XtraEditors.LabelControl lbResourceCount;
        private DevExpress.XtraEditors.SpinEdit spinResourceCount;
        protected DevExpress.XtraEditors.DateEdit edtEnd;
        protected DevExpress.XtraEditors.DateEdit edtStart;
        protected DevExpress.XtraEditors.LabelControl lblEnd;
        protected DevExpress.XtraEditors.LabelControl lblStart;

        TimeInterval printInterval = TimeInterval.Empty;
		int pagesPerWeek;
        int resourceCount;

		public new Report Report { get { return (Report)base.Report; } }
        private DateTime EndDate { get { return edtEnd.DateTime.AddDays(1); } set { edtEnd.DateTime = value.AddDays(-1); } }
        private DateTime StartDate { get { return edtStart.DateTime; } set { edtStart.DateTime = value; } }

		public PreviewControl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();			
			InitializeControlValues();
            SubscribeEvents();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		protected override XtraSchedulerReport CreateReportInstance() {
			return new Report();
		}
		protected override void InitAdapterTimeInterval() {
			StoragePrintAdapter.TimeInterval = new TimeInterval(DemoUtils.Date, TimeSpan.FromDays(14));
		}
		void InitializeControlValues() {
            printInterval = new TimeInterval(DemoUtils.Date, DemoUtils.Date.AddDays(6));
            StartDate = printInterval.Start;
            EndDate = printInterval.End;

			this.pagesPerWeek = Report.ColumnCount;
            this.cmbLayout.SelectedIndex = pagesPerWeek - 1;
            InitializeResourceCount();
		}
        void InitializeResourceCount() {
            this.spinResourceCount.Properties.MinValue = 1;
            this.spinResourceCount.Properties.MaxValue = schedulerStorage.Resources.Count;
            this.resourceCount = Report.ResourceCount;
            this.spinResourceCount.Value = this.resourceCount;
        }
		protected override void UpdateReportProperties(XtraSchedulerReport report) {
            StoragePrintAdapter.TimeInterval = printInterval;
			base.UpdateReportProperties(report);
			Report.ColumnCount = pagesPerWeek;
            Report.ResourceCount = resourceCount;
		}
		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lbLayout = new DevExpress.XtraEditors.LabelControl();
            this.cmbLayout = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbResourceCount = new DevExpress.XtraEditors.LabelControl();
            this.spinResourceCount = new DevExpress.XtraEditors.SpinEdit();
            this.edtEnd = new DevExpress.XtraEditors.DateEdit();
            this.edtStart = new DevExpress.XtraEditors.DateEdit();
            this.lblEnd = new DevExpress.XtraEditors.LabelControl();
            this.lblStart = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            this.pnlSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbLayout.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinResourceCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEnd.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStart.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStart.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // smplEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(620, 35);
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.edtEnd);
            this.pnlSettings.Controls.Add(this.edtStart);
            this.pnlSettings.Controls.Add(this.lblEnd);
            this.pnlSettings.Controls.Add(this.lblStart);
            this.pnlSettings.Controls.Add(this.lbResourceCount);
            this.pnlSettings.Controls.Add(this.spinResourceCount);
            this.pnlSettings.Controls.Add(this.cmbLayout);
            this.pnlSettings.Controls.Add(this.lbLayout);
            this.pnlSettings.Size = new System.Drawing.Size(700, 68);
            this.pnlSettings.Controls.SetChildIndex(this.lbLayout, 0);
            this.pnlSettings.Controls.SetChildIndex(this.cmbLayout, 0);
            this.pnlSettings.Controls.SetChildIndex(this.btnEdit, 0);
            this.pnlSettings.Controls.SetChildIndex(this.spinResourceCount, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lbResourceCount, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lblStart, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lblEnd, 0);
            this.pnlSettings.Controls.SetChildIndex(this.edtStart, 0);
            this.pnlSettings.Controls.SetChildIndex(this.edtEnd, 0);
            // 
            // panelSeparatorControl
            // 
            this.panelSeparatorControl.Location = new System.Drawing.Point(0, 68);
            // 
            // printControl
            // 
            this.printControl.Location = new System.Drawing.Point(0, 76);
            this.printControl.Size = new System.Drawing.Size(700, 320);
            // 
            // lbLayout
            // 
            this.lbLayout.Location = new System.Drawing.Point(9, 41);
            this.lbLayout.Name = "lbLayout";
            this.lbLayout.Size = new System.Drawing.Size(37, 13);
            this.lbLayout.TabIndex = 4;
            this.lbLayout.Text = "Layout:";
            // 
            // cmbLayout
            // 
            this.cmbLayout.EditValue = "1 page/week";
            this.cmbLayout.Location = new System.Drawing.Point(95, 37);
            this.cmbLayout.Name = "cmbLayout";
            this.cmbLayout.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbLayout.Properties.Items.AddRange(new object[] {
            "1 page/week",
            "2 pages/week"});
            this.cmbLayout.Size = new System.Drawing.Size(85, 20);
            this.cmbLayout.TabIndex = 5;
            this.cmbLayout.SelectedIndexChanged += new System.EventHandler(this.cmbLayout_SelectedIndexChanged);
            // 
            // lbResourceCount
            // 
            this.lbResourceCount.Location = new System.Drawing.Point(9, 15);
            this.lbResourceCount.Name = "lbResourceCount";
            this.lbResourceCount.Size = new System.Drawing.Size(81, 13);
            this.lbResourceCount.TabIndex = 43;
            this.lbResourceCount.Text = "Resource Count:";
            // 
            // spinResourceCount
            // 
            this.spinResourceCount.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinResourceCount.Location = new System.Drawing.Point(95, 11);
            this.spinResourceCount.Name = "spinResourceCount";
            this.spinResourceCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinResourceCount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinResourceCount.Properties.IsFloatValue = false;
            this.spinResourceCount.Properties.Mask.EditMask = "N00";
            this.spinResourceCount.Properties.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spinResourceCount.Size = new System.Drawing.Size(85, 20);
            this.spinResourceCount.TabIndex = 42;
            // 
            // edtEnd
            // 
            this.edtEnd.EditValue = new System.DateTime(2005, 9, 5, 0, 0, 0, 0);
            this.edtEnd.Location = new System.Drawing.Point(256, 37);
            this.edtEnd.Name = "edtEnd";
            this.edtEnd.Properties.AccessibleName = "End date:";
            this.edtEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.edtEnd.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.edtEnd.Size = new System.Drawing.Size(86, 20);
            this.edtEnd.TabIndex = 47;
            // 
            // edtStart
            // 
            this.edtStart.EditValue = new System.DateTime(2005, 9, 5, 0, 0, 0, 0);
            this.edtStart.Location = new System.Drawing.Point(256, 11);
            this.edtStart.Name = "edtStart";
            this.edtStart.Properties.AccessibleName = "Start date:";
            this.edtStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.edtStart.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.edtStart.Size = new System.Drawing.Size(86, 20);
            this.edtStart.TabIndex = 46;
            // 
            // lblEnd
            // 
            this.lblEnd.AccessibleName = "End date:";
            this.lblEnd.Location = new System.Drawing.Point(198, 41);
            this.lblEnd.Name = "lblEnd";
            this.lblEnd.Size = new System.Drawing.Size(47, 13);
            this.lblEnd.TabIndex = 44;
            this.lblEnd.Text = "End date:";
            // 
            // lblStart
            // 
            this.lblStart.AccessibleName = "Start date:";
            this.lblStart.Location = new System.Drawing.Point(198, 15);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(53, 13);
            this.lblStart.TabIndex = 45;
            this.lblStart.Text = "Start date:";
            // 
            // PreviewControl
            // 
            this.Name = "PreviewControl";
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            this.pnlSettings.ResumeLayout(false);
            this.pnlSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbLayout.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinResourceCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEnd.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStart.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStart.Properties)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

        protected internal virtual void UnsubscribeEvents() {
            this.edtStart.EditValueChanged -= new EventHandler(StartEditValueChanged);
            this.edtEnd.EditValueChanged -= new EventHandler(EndEditValueChanged);
            this.spinResourceCount.EditValueChanged -= new EventHandler(spinResourceCount_EditValueChanged);
        }
        protected internal virtual void SubscribeEvents() {
            this.edtStart.EditValueChanged += new EventHandler(StartEditValueChanged);
            this.edtEnd.EditValueChanged += new EventHandler(EndEditValueChanged);
            this.spinResourceCount.EditValueChanged += new EventHandler(spinResourceCount_EditValueChanged);
        }

        protected internal virtual bool IsValidInterval(DateTime start, DateTime end) {
            return start <= end;
        }
        private void StartEditValueChanged(object sender, EventArgs e) {
            UnsubscribeEvents();
            if (!IsValidInterval(StartDate, EndDate))
                edtEnd.EditValue = StartDate;

            this.printInterval = new TimeInterval(StartDate, EndDate);
            SubscribeEvents();
            UpdateActiveReport();
        }
        
        void spinResourceCount_EditValueChanged(object sender, EventArgs e) {
            this.resourceCount = (int)this.spinResourceCount.Value;
            UpdateActiveReport();
        }
        void EndEditValueChanged(object sender, EventArgs e) {
            UnsubscribeEvents();
            if (!IsValidInterval(StartDate, EndDate))
                edtStart.EditValue = EndDate.AddDays(-1);

            this.printInterval = new TimeInterval(StartDate, EndDate);
            SubscribeEvents();
            UpdateActiveReport();
        }
		void cmbLayout_SelectedIndexChanged(object sender, EventArgs e) {
			this.pagesPerWeek = cmbLayout.SelectedIndex + 1;
			UpdateActiveReport();
		}
	}
}

