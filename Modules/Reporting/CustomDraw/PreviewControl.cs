using System;
using DevExpress.XtraScheduler.Reporting;
using DevExpress.XtraEditors;

namespace DevExpress.XtraScheduler.Demos.Reporting.CustomDraw {

	public class PreviewControl : SingleReportPreviewControl {
        private System.ComponentModel.IContainer components = null;

        bool allowCustomDrawDayHeader;
        bool allowCustomDrawResourceHeader;
        bool allowCustomDrawAllDayArea;
        bool allowCustomDrawTimeCell;
        bool allowCustomDrawTimeRuler;
        bool allowCustomDrawAppointment;
        private CheckEdit chkTimeRuler;
        private CheckEdit chkAppointmentBackground;
        private CheckEdit chkAppointment;
        private CheckEdit chkDayViewAllDayArea;
        private CheckEdit chkTimeCell;
        private CheckEdit chkDayHeader;
        private CheckEdit chkResourceHeader;
        bool allowCustomDrawAppointmentBackground;


		public PreviewControl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();			
			InitializeControlValues();            
		}

		public new Report Report { get { return (Report)base.Report; } }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		protected override XtraSchedulerReport CreateReportInstance() {
			return new Report();
		}
        protected override void InitAdapterTimeInterval() {
            StoragePrintAdapter.TimeInterval = new TimeInterval(DemoUtils.Date, TimeSpan.FromDays(7));
        }
		void InitializeControlValues() {
            allowCustomDrawDayHeader = chkDayHeader.Checked;
            allowCustomDrawResourceHeader = chkResourceHeader.Checked;
            allowCustomDrawAllDayArea = chkDayViewAllDayArea.Checked;
            allowCustomDrawTimeCell = chkTimeCell.Checked;
            allowCustomDrawTimeRuler = chkTimeRuler.Checked;
            allowCustomDrawAppointment = chkAppointment.Checked;
            allowCustomDrawAppointmentBackground = chkAppointmentBackground.Checked;
		}
		protected override void UpdateReportProperties(XtraSchedulerReport report) {
			base.UpdateReportProperties(report);
            Report.AllowCustomDrawAllDayArea = allowCustomDrawAllDayArea;
            Report.AllowCustomDrawAppointment = allowCustomDrawAppointment;
            Report.AllowCustomDrawAppointmentBackground = allowCustomDrawAppointmentBackground;
            Report.AllowCustomDrawDayHeader = allowCustomDrawDayHeader;
            Report.AllowCustomDrawResourceHeader = allowCustomDrawResourceHeader;
            Report.AllowCustomDrawTimeCell = allowCustomDrawTimeCell;
            Report.AllowCustomDrawTimeRuler = allowCustomDrawTimeRuler;
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.chkTimeRuler = new DevExpress.XtraEditors.CheckEdit();
            this.chkAppointmentBackground = new DevExpress.XtraEditors.CheckEdit();
            this.chkAppointment = new DevExpress.XtraEditors.CheckEdit();
            this.chkDayViewAllDayArea = new DevExpress.XtraEditors.CheckEdit();
            this.chkTimeCell = new DevExpress.XtraEditors.CheckEdit();
            this.chkDayHeader = new DevExpress.XtraEditors.CheckEdit();
            this.chkResourceHeader = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).BeginInit();

            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            this.pnlSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTimeRuler.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAppointmentBackground.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAppointment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDayViewAllDayArea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTimeCell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDayHeader.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkResourceHeader.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // smplEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(616, 30);
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.chkTimeRuler);
            this.pnlSettings.Controls.Add(this.chkAppointmentBackground);
            this.pnlSettings.Controls.Add(this.chkAppointment);
            this.pnlSettings.Controls.Add(this.chkDayViewAllDayArea);
            this.pnlSettings.Controls.Add(this.chkTimeCell);
            this.pnlSettings.Controls.Add(this.chkDayHeader);
            this.pnlSettings.Controls.Add(this.chkResourceHeader);
            this.pnlSettings.Size = new System.Drawing.Size(700, 60);
            this.pnlSettings.Controls.SetChildIndex(this.btnEdit, 0);
            this.pnlSettings.Controls.SetChildIndex(this.chkResourceHeader, 0);
            this.pnlSettings.Controls.SetChildIndex(this.chkDayHeader, 0);
            this.pnlSettings.Controls.SetChildIndex(this.chkTimeCell, 0);
            this.pnlSettings.Controls.SetChildIndex(this.chkDayViewAllDayArea, 0);
            this.pnlSettings.Controls.SetChildIndex(this.chkAppointment, 0);
            this.pnlSettings.Controls.SetChildIndex(this.chkAppointmentBackground, 0);
            this.pnlSettings.Controls.SetChildIndex(this.chkTimeRuler, 0);
            // 
            // panelSeparatorControl
            // 
            this.panelSeparatorControl.Location = new System.Drawing.Point(0, 60);
            // 
            // printControl
            // 
            this.printControl.Location = new System.Drawing.Point(0, 68);
            this.printControl.Size = new System.Drawing.Size(700, 328);
            // 
            // chkTimeRuler
            // 
            this.chkTimeRuler.EditValue = true;
            this.chkTimeRuler.Location = new System.Drawing.Point(12, 8);
            this.chkTimeRuler.Name = "chkTimeRuler";
            this.chkTimeRuler.Properties.Caption = "Time Ruler";
            this.chkTimeRuler.Size = new System.Drawing.Size(85, 19);
            this.chkTimeRuler.TabIndex = 101;
            this.chkTimeRuler.CheckedChanged += new System.EventHandler(this.chkTimeRuler_CheckedChanged);
            // 
            // chkAppointmentBackground
            // 
            this.chkAppointmentBackground.EditValue = true;
            this.chkAppointmentBackground.Location = new System.Drawing.Point(364, 8);
            this.chkAppointmentBackground.Name = "chkAppointmentBackground";
            this.chkAppointmentBackground.Properties.Caption = "Appointment Background";
            this.chkAppointmentBackground.Size = new System.Drawing.Size(149, 19);
            this.chkAppointmentBackground.TabIndex = 100;
            this.chkAppointmentBackground.CheckedChanged += new System.EventHandler(this.chkAppointmentBackground_CheckedChanged);
            // 
            // chkAppointment
            // 
            this.chkAppointment.EditValue = true;
            this.chkAppointment.Location = new System.Drawing.Point(257, 8);
            this.chkAppointment.Name = "chkAppointment";
            this.chkAppointment.Properties.Caption = "Appointment";
            this.chkAppointment.Size = new System.Drawing.Size(101, 19);
            this.chkAppointment.TabIndex = 99;
            this.chkAppointment.CheckedChanged += new System.EventHandler(this.chkAppointment_CheckedChanged);
            // 
            // chkDayViewAllDayArea
            // 
            this.chkDayViewAllDayArea.EditValue = true;
            this.chkDayViewAllDayArea.Location = new System.Drawing.Point(12, 33);
            this.chkDayViewAllDayArea.Name = "chkDayViewAllDayArea";
            this.chkDayViewAllDayArea.Properties.Caption = "All-Day Area";
            this.chkDayViewAllDayArea.Size = new System.Drawing.Size(85, 19);
            this.chkDayViewAllDayArea.TabIndex = 98;
            this.chkDayViewAllDayArea.CheckedChanged += new System.EventHandler(this.chkDayViewAllDayArea_CheckedChanged);
            // 
            // chkTimeCell
            // 
            this.chkTimeCell.EditValue = true;
            this.chkTimeCell.Location = new System.Drawing.Point(257, 33);
            this.chkTimeCell.Name = "chkTimeCell";
            this.chkTimeCell.Properties.Caption = "Time Cell";
            this.chkTimeCell.Size = new System.Drawing.Size(72, 19);
            this.chkTimeCell.TabIndex = 97;
            this.chkTimeCell.CheckedChanged += new System.EventHandler(this.chkTimeCell_CheckedChanged);
            // 
            // chkDayHeader
            // 
            this.chkDayHeader.EditValue = true;
            this.chkDayHeader.Location = new System.Drawing.Point(131, 8);
            this.chkDayHeader.Name = "chkDayHeader";
            this.chkDayHeader.Properties.Caption = "Day Header";
            this.chkDayHeader.Size = new System.Drawing.Size(88, 19);
            this.chkDayHeader.TabIndex = 96;
            this.chkDayHeader.CheckedChanged += new System.EventHandler(this.chkDayHeader_CheckedChanged);
            // 
            // chkResourceHeader
            // 
            this.chkResourceHeader.EditValue = true;
            this.chkResourceHeader.Location = new System.Drawing.Point(131, 32);
            this.chkResourceHeader.Name = "chkResourceHeader";
            this.chkResourceHeader.Properties.Caption = "Resource Header";
            this.chkResourceHeader.Size = new System.Drawing.Size(107, 19);
            this.chkResourceHeader.TabIndex = 95;
            this.chkResourceHeader.CheckedChanged += new System.EventHandler(this.chkResourceHeader_CheckedChanged);
            // 
            // PreviewControl
            // 
            this.Name = "PreviewControl";
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            this.pnlSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTimeRuler.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAppointmentBackground.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAppointment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDayViewAllDayArea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTimeCell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDayHeader.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkResourceHeader.Properties)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion


        private void chkDayHeader_CheckedChanged(object sender, EventArgs e) {
            this.allowCustomDrawDayHeader = ((CheckEdit)sender).Checked;
            UpdateActiveReport();
        }

        private void chkResourceHeader_CheckedChanged(object sender, EventArgs e) {
            this.allowCustomDrawResourceHeader = ((CheckEdit)sender).Checked;
            UpdateActiveReport();
        }

        private void chkTimeCell_CheckedChanged(object sender, EventArgs e) {
            this.allowCustomDrawTimeCell = ((CheckEdit)sender).Checked;
            UpdateActiveReport();

        }

        private void chkDayViewAllDayArea_CheckedChanged(object sender, EventArgs e) {
            this.allowCustomDrawAllDayArea = ((CheckEdit)sender).Checked;
            UpdateActiveReport();
        }

        private void chkAppointment_CheckedChanged(object sender, EventArgs e) {
            this.allowCustomDrawAppointment = ((CheckEdit)sender).Checked;
            UpdateActiveReport();

        }

        private void chkAppointmentBackground_CheckedChanged(object sender, EventArgs e) {
            this.allowCustomDrawAppointmentBackground = ((CheckEdit)sender).Checked;
            UpdateActiveReport();
        }

        private void chkTimeRuler_CheckedChanged(object sender, EventArgs e) {
            this.allowCustomDrawTimeRuler = ((CheckEdit)sender).Checked;
            UpdateActiveReport();
        }
	}

}

