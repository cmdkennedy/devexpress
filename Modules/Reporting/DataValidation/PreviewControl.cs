using System;
using System.Collections;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Reporting;
using System.Collections.Generic;

namespace DevExpress.XtraScheduler.Demos.Reporting.DataValidation {

	public class PreviewControl : SingleReportPreviewControl {
        private System.ComponentModel.IContainer components = null;
        private DevExpress.XtraEditors.LabelControl lbResources;
        private DevExpress.XtraEditors.ComboBoxEdit cmbResources;
        private DevExpress.XtraEditors.LabelControl lblAppointments;
        private DevExpress.XtraEditors.ImageComboBoxEdit cmbDays;
        private DevExpress.XtraEditors.LabelControl lblTimeIntervals;
        private DevExpress.XtraEditors.ImageComboBoxEdit cmbAppointments;

		public new Report Report { get { return (Report)base.Report; } }

		public PreviewControl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
            UpdateActiveReport();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		protected override XtraSchedulerReport CreateReportInstance() {
			return new Report();
		}
		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lbResources = new DevExpress.XtraEditors.LabelControl();
            this.cmbResources = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblAppointments = new DevExpress.XtraEditors.LabelControl();
            this.cmbAppointments = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.cmbDays = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.lblTimeIntervals = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            this.pnlSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbResources.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAppointments.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDays.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // storagePrintAdapter
            // 
            this.storagePrintAdapter.ValidateAppointments += new DevExpress.XtraScheduler.Reporting.AppointmentsValidationEventHandler(this.storagePrintAdapter_ValidateAppointments);
            this.storagePrintAdapter.ValidateTimeIntervals += new DevExpress.XtraScheduler.Reporting.TimeIntervalsValidationEventHandler(this.storagePrintAdapter_ValidateTimeIntervals);
            this.storagePrintAdapter.ValidateResources += new DevExpress.XtraScheduler.Reporting.ResourcesValidationEventHandler(this.storagePrintAdapter_ValidateResources);
            // 
            // smplEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(620, 9);
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.cmbDays);
            this.pnlSettings.Controls.Add(this.lblTimeIntervals);
            this.pnlSettings.Controls.Add(this.cmbAppointments);
            this.pnlSettings.Controls.Add(this.lblAppointments);
            this.pnlSettings.Controls.Add(this.lbResources);
            this.pnlSettings.Controls.Add(this.cmbResources);
            this.pnlSettings.Size = new System.Drawing.Size(700, 42);
            this.pnlSettings.Controls.SetChildIndex(this.btnEdit, 0);
            this.pnlSettings.Controls.SetChildIndex(this.cmbResources, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lbResources, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lblAppointments, 0);
            this.pnlSettings.Controls.SetChildIndex(this.cmbAppointments, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lblTimeIntervals, 0);
            this.pnlSettings.Controls.SetChildIndex(this.cmbDays, 0);
            // 
            // panelSeparatorControl
            // 
            this.panelSeparatorControl.Location = new System.Drawing.Point(0, 42);
            // 
            // printControl
            // 
            this.printControl.Location = new System.Drawing.Point(0, 50);
            this.printControl.Size = new System.Drawing.Size(700, 346);
            // 
            // lbResources
            // 
            this.lbResources.Location = new System.Drawing.Point(11, 15);
            this.lbResources.Name = "lbResources";
            this.lbResources.Size = new System.Drawing.Size(54, 13);
            this.lbResources.TabIndex = 12;
            this.lbResources.Text = "Resources:";
            // 
            // cmbResources
            // 
            this.cmbResources.EditValue = "All";
            this.cmbResources.Location = new System.Drawing.Point(70, 11);
            this.cmbResources.Name = "cmbResources";
            this.cmbResources.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbResources.Properties.Items.AddRange(new object[] {
            "All",
            "Top 3 ",
            "A-Z order",
            "Z-A order",
            ""});
            this.cmbResources.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbResources.Size = new System.Drawing.Size(70, 20);
            this.cmbResources.TabIndex = 13;
            this.cmbResources.SelectedIndexChanged += new System.EventHandler(this.cmbResources_SelectedIndexChanged);
            // 
            // lblAppointments
            // 
            this.lblAppointments.Location = new System.Drawing.Point(158, 15);
            this.lblAppointments.Name = "lblAppointments";
            this.lblAppointments.Size = new System.Drawing.Size(70, 13);
            this.lblAppointments.TabIndex = 14;
            this.lblAppointments.Text = "Appointments:";
            // 
            // cmbAppointments
            // 
            this.cmbAppointments.EditValue = DevExpress.XtraScheduler.UsedAppointmentType.All;
            this.cmbAppointments.Location = new System.Drawing.Point(233, 11);
            this.cmbAppointments.Name = "cmbAppointments";
            this.cmbAppointments.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbAppointments.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("All", DevExpress.XtraScheduler.UsedAppointmentType.All, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Recurring", DevExpress.XtraScheduler.UsedAppointmentType.Recurring, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("NonRecurring", DevExpress.XtraScheduler.UsedAppointmentType.NonRecurring, -1)});
            this.cmbAppointments.Size = new System.Drawing.Size(84, 20);
            this.cmbAppointments.TabIndex = 38;
            this.cmbAppointments.SelectedIndexChanged += new System.EventHandler(this.cmbAppointments_SelectedIndexChanged);
            // 
            // cmbDays
            // 
            this.cmbDays.EditValue = DevExpress.XtraScheduler.WeekDays.EveryDay;
            this.cmbDays.Location = new System.Drawing.Point(368, 11);
            this.cmbDays.Name = "cmbDays";
            this.cmbDays.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDays.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Every Day", DevExpress.XtraScheduler.WeekDays.EveryDay, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Work Days", DevExpress.XtraScheduler.WeekDays.WorkDays, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Weekend Days", DevExpress.XtraScheduler.WeekDays.WeekendDays, -1)});
            this.cmbDays.Size = new System.Drawing.Size(96, 20);
            this.cmbDays.TabIndex = 40;
            this.cmbDays.SelectedIndexChanged += new System.EventHandler(this.cmbDays_SelectedIndexChanged);
            // 
            // lblTimeIntervals
            // 
            this.lblTimeIntervals.Location = new System.Drawing.Point(335, 15);
            this.lblTimeIntervals.Name = "lblTimeIntervals";
            this.lblTimeIntervals.Size = new System.Drawing.Size(28, 13);
            this.lblTimeIntervals.TabIndex = 39;
            this.lblTimeIntervals.Text = "Days:";
            // 
            // PreviewControl
            // 
            this.Name = "PreviewControl";
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            this.pnlSettings.ResumeLayout(false);
            this.pnlSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbResources.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAppointments.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDays.Properties)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

        protected override void InitAdapterTimeInterval() {
            StoragePrintAdapter.TimeInterval = new TimeInterval(DemoUtils.Date, TimeSpan.FromDays(10));
        }

		private void storagePrintAdapter_ValidateAppointments(object sender, AppointmentsValidationEventArgs e) {
            UsedAppointmentType aptType = (UsedAppointmentType)cmbAppointments.EditValue;
            if (aptType == UsedAppointmentType.All)
                return;

            if (aptType == UsedAppointmentType.None) {
                e.Appointments.Clear();
                return;
            }
			int count = e.Appointments.Count;
			AppointmentBaseCollection result = new AppointmentBaseCollection();
			for (int i = 0; i < count; i++) {
				Appointment apt = e.Appointments[i];
                if (aptType == UsedAppointmentType.Recurring && apt.IsRecurring)
                    result.Add(apt);
                if (aptType == UsedAppointmentType.NonRecurring && !apt.IsRecurring)
                    result.Add(apt);
			}
			e.Appointments.Clear();
			e.Appointments.AddRange(result);
		}

        private void storagePrintAdapter_ValidateResources(object sender, ResourcesValidationEventArgs e) {
            int resourceMode = cmbResources.SelectedIndex;
            if (resourceMode == 1) {
                int count = e.Resources.Count;
                for (int i = count - 1; i >= 3; i--) {
                    e.Resources.RemoveAt(i);
                }
            }
            if (resourceMode == 2) {
                e.Resources.Sort(new ResourceCaptionComparer());
            }
            if (resourceMode == 3) {
                e.Resources.Sort(new ResourceCaptionReverseComparer());
            }
        }
        
		private void storagePrintAdapter_ValidateTimeIntervals(object sender, TimeIntervalsValidationEventArgs e) {
            WeekDays weekDays = (WeekDays)cmbDays.EditValue;
			if (weekDays == WeekDays.EveryDay)
				return;

			int count = e.Intervals.Count;

            DayIntervalCollection result = new DayIntervalCollection();
			for (int i = count - 1; i >= 0; i--) {
                DayIntervalCollection sourceDays = new DayIntervalCollection();
                sourceDays.Add(e.Intervals[i]);
                FilterDays(sourceDays, weekDays);
                result.AddRange(sourceDays);
			}

			e.Intervals.Clear();
            for (int i = 0; i < result.Count; i++) {
                e.Intervals.Add(result[i]);    
            }
            
		}
        void FilterDays(DayIntervalCollection dayIntervals, WeekDays validDays) {
            int count = dayIntervals.Count;
            for (int i = count - 1; i >= 0; i--) {
                DayOfWeek day = dayIntervals[i].Start.DayOfWeek;
                WeekDays weekDay = DevExpress.XtraScheduler.Native.DateTimeHelper.ToWeekDays(day);
                if ((weekDay & validDays) != weekDay)
                    dayIntervals.RemoveAt(i);
            }
        }
        private void cmbResources_SelectedIndexChanged(object sender, EventArgs e) {
            UpdateActiveReport();
        }

        private void cmbAppointments_SelectedIndexChanged(object sender, EventArgs e) {
            UpdateActiveReport();
        }

        private void cmbDays_SelectedIndexChanged(object sender, EventArgs e) {
            CreateReports();
            UpdateActiveReport();
        }

	}
    public class ResourceCaptionComparer : IComparer<Resource>, IComparer {

        #region IComparer Members
        int IComparer.Compare(object x, object y) {
            return CompareCore(x, y);
        }
		public int Compare(Resource x, Resource y) {
			return CompareCore(x, y);
		}
        #endregion

        protected virtual int CompareCore(object x, object y) {
            Resource xRes = (Resource)x;
            Resource yRes = (Resource)y;
            if (xRes == null || yRes == null)
                return 0;
            if (Resource.Empty.Equals(xRes) || Resource.Empty.Equals(yRes))
                return 0;
            return CompareCaptions(xRes, yRes);
        }
        protected virtual int CompareCaptions(Resource xRes, Resource yRes) {
            return String.Compare(xRes.Caption, yRes.Caption);
        }

    }
    public class ResourceCaptionReverseComparer : ResourceCaptionComparer {
        protected override int CompareCaptions(Resource xRes, Resource yRes) {
            return String.Compare(yRes.Caption, xRes.Caption);
        }
    }
}

