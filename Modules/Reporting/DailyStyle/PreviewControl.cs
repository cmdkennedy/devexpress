using System;
using DevExpress.XtraScheduler.Reporting;
using DevExpress.XtraScheduler.Native;

namespace DevExpress.XtraScheduler.Demos.Reporting.DailyStyle {

	public class PreviewControl : SingleReportPreviewControl {
        private System.ComponentModel.IContainer components = null;
        protected DevExpress.XtraEditors.CheckEdit chkPrintAllAppointments;
        protected DevExpress.XtraScheduler.UI.DurationEdit cbTimeScale;
        protected DevExpress.XtraEditors.LabelControl lblTimeScale;
        protected DevExpress.XtraScheduler.UI.SchedulerTimeEdit steToTime;
        protected DevExpress.XtraScheduler.UI.SchedulerTimeEdit steFromTime;
        protected DevExpress.XtraEditors.LabelControl lblPrintTo;
        protected DevExpress.XtraEditors.LabelControl lblPrintFrom;
        protected DevExpress.XtraEditors.DateEdit edtEnd;
        protected DevExpress.XtraEditors.DateEdit edtStart;
        protected DevExpress.XtraEditors.LabelControl lblEnd;
        protected DevExpress.XtraEditors.LabelControl lblStart;
        DevExpress.XtraEditors.LabelControl lbResourceCount;
        DevExpress.XtraEditors.SpinEdit spinResourceCount;

        int visibleResourceCount = 0;
        int visibleDayCount = 1;
        bool printAllAppointments = false;
        TimeSpan timeScale = TimeSpan.Zero;
        TimeInterval printInterval = TimeInterval.Empty;
        DevExpress.XtraEditors.LabelControl labelControl1;
        DevExpress.XtraEditors.SpinEdit spinDayCount;
        TimeOfDayInterval visibleTime = TimeOfDayInterval.Day;
        
        //SchedulerGroupType groupType = SchedulerGroupType.None;

		public new Report Report { get { return (Report)base.Report; } }
        private DateTime EndDate { get { return edtEnd.DateTime.AddDays(1); } set { edtEnd.DateTime = value.AddDays(-1); } }
        private DateTime StartDate { get { return edtStart.DateTime; } set { edtStart.DateTime = value; } }

		public PreviewControl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
            InitializeControlValues();
            SubscribeEvents();         
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		protected override XtraSchedulerReport CreateReportInstance() {
			return new Report();
		}        
		void InitializeControlValues() {
            this.visibleResourceCount = Report.VisibleResourceCount;
            this.spinResourceCount.EditValue = visibleResourceCount;
            this.spinResourceCount.Properties.MaxValue = StoragePrintAdapter.ResourceCount;

            this.visibleDayCount = Report.VisibleDayCount;
            this.spinDayCount.EditValue = this.visibleDayCount;
            this.spinDayCount.Properties.MinValue = 1;
            this.spinDayCount.Properties.MaxValue = 7;

            visibleTime = Report.VisibleTime;
            steFromTime.Time = new DateTime(visibleTime.Start.Ticks);
            steToTime.Time = new DateTime(visibleTime.End.Ticks);

            printInterval = new TimeInterval(DemoUtils.Date, DemoUtils.Date.AddDays(6));
            StartDate = printInterval.Start;
            EndDate = printInterval.End;

            timeScale = Report.TimeScale;
            cbTimeScale.EditValue = timeScale;

            printAllAppointments = Report.PrintAllAppointments;
            chkPrintAllAppointments.Checked = printAllAppointments;

		}
		protected override void UpdateReportProperties(XtraSchedulerReport report) {
            StoragePrintAdapter.TimeInterval = printInterval;
            base.UpdateReportProperties(report);

			Report.VisibleResourceCount = this.visibleResourceCount;
            Report.VisibleTime = this.visibleTime;
            Report.TimeScale = this.timeScale;
            Report.VisibleDayCount = this.visibleDayCount;

            Report.PrintAllAppointments = printAllAppointments;
		}
		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.chkPrintAllAppointments = new DevExpress.XtraEditors.CheckEdit();
            this.edtEnd = new DevExpress.XtraEditors.DateEdit();
            this.edtStart = new DevExpress.XtraEditors.DateEdit();
            this.lblEnd = new DevExpress.XtraEditors.LabelControl();
            this.lblStart = new DevExpress.XtraEditors.LabelControl();
            this.cbTimeScale = new DevExpress.XtraScheduler.UI.DurationEdit();
            this.lblTimeScale = new DevExpress.XtraEditors.LabelControl();
            this.steToTime = new DevExpress.XtraScheduler.UI.SchedulerTimeEdit();
            this.steFromTime = new DevExpress.XtraScheduler.UI.SchedulerTimeEdit();
            this.lblPrintTo = new DevExpress.XtraEditors.LabelControl();
            this.lblPrintFrom = new DevExpress.XtraEditors.LabelControl();
            this.lbResourceCount = new DevExpress.XtraEditors.LabelControl();
            this.spinResourceCount = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.spinDayCount = new DevExpress.XtraEditors.SpinEdit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            this.pnlSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintAllAppointments.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEnd.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStart.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTimeScale.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.steToTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.steFromTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinResourceCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinDayCount.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // smplEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(620, 61);
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.labelControl1);
            this.pnlSettings.Controls.Add(this.lbResourceCount);
            this.pnlSettings.Controls.Add(this.spinDayCount);
            this.pnlSettings.Controls.Add(this.spinResourceCount);
            this.pnlSettings.Controls.Add(this.cbTimeScale);
            this.pnlSettings.Controls.Add(this.lblTimeScale);
            this.pnlSettings.Controls.Add(this.steToTime);
            this.pnlSettings.Controls.Add(this.steFromTime);
            this.pnlSettings.Controls.Add(this.lblPrintTo);
            this.pnlSettings.Controls.Add(this.lblPrintFrom);
            this.pnlSettings.Controls.Add(this.edtEnd);
            this.pnlSettings.Controls.Add(this.edtStart);
            this.pnlSettings.Controls.Add(this.lblEnd);
            this.pnlSettings.Controls.Add(this.lblStart);
            this.pnlSettings.Controls.Add(this.chkPrintAllAppointments);
            this.pnlSettings.Size = new System.Drawing.Size(700, 94);
            this.pnlSettings.Controls.SetChildIndex(this.btnEdit, 0);
            this.pnlSettings.Controls.SetChildIndex(this.chkPrintAllAppointments, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lblStart, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lblEnd, 0);
            this.pnlSettings.Controls.SetChildIndex(this.edtStart, 0);
            this.pnlSettings.Controls.SetChildIndex(this.edtEnd, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lblPrintFrom, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lblPrintTo, 0);
            this.pnlSettings.Controls.SetChildIndex(this.steFromTime, 0);
            this.pnlSettings.Controls.SetChildIndex(this.steToTime, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lblTimeScale, 0);
            this.pnlSettings.Controls.SetChildIndex(this.cbTimeScale, 0);
            this.pnlSettings.Controls.SetChildIndex(this.spinResourceCount, 0);
            this.pnlSettings.Controls.SetChildIndex(this.spinDayCount, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lbResourceCount, 0);
            this.pnlSettings.Controls.SetChildIndex(this.labelControl1, 0);
            // 
            // panelSeparatorControl
            // 
            this.panelSeparatorControl.Location = new System.Drawing.Point(0, 94);
            // 
            // printControl
            // 
            this.printControl.Location = new System.Drawing.Point(0, 102);
            this.printControl.Size = new System.Drawing.Size(700, 294);
            // 
            // chkPrintAllAppointments
            // 
            this.chkPrintAllAppointments.Location = new System.Drawing.Point(198, 64);
            this.chkPrintAllAppointments.Name = "chkPrintAllAppointments";
            this.chkPrintAllAppointments.Properties.AccessibleName = "Print all appointments";
            this.chkPrintAllAppointments.Properties.AutoWidth = true;
            this.chkPrintAllAppointments.Properties.Caption = "Print all appointments";
            this.chkPrintAllAppointments.Size = new System.Drawing.Size(126, 19);
            this.chkPrintAllAppointments.TabIndex = 8;
            this.chkPrintAllAppointments.CheckedChanged += new System.EventHandler(this.chkPrintAllAppointments_CheckedChanged);
            // 
            // edtEnd
            // 
            this.edtEnd.EditValue = new System.DateTime(2005, 9, 5, 0, 0, 0, 0);
            this.edtEnd.Location = new System.Drawing.Point(256, 37);
            this.edtEnd.Name = "edtEnd";
            this.edtEnd.Properties.AccessibleName = "End date:";
            this.edtEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.edtEnd.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.edtEnd.Size = new System.Drawing.Size(86, 20);
            this.edtEnd.TabIndex = 16;
            // 
            // edtStart
            // 
            this.edtStart.EditValue = new System.DateTime(2005, 9, 5, 0, 0, 0, 0);
            this.edtStart.Location = new System.Drawing.Point(256, 11);
            this.edtStart.Name = "edtStart";
            this.edtStart.Properties.AccessibleName = "Start date:";
            this.edtStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.edtStart.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.edtStart.Size = new System.Drawing.Size(86, 20);
            this.edtStart.TabIndex = 15;
            // 
            // lblEnd
            // 
            this.lblEnd.AccessibleName = "End date:";
            this.lblEnd.Location = new System.Drawing.Point(198, 41);
            this.lblEnd.Name = "lblEnd";
            this.lblEnd.Size = new System.Drawing.Size(47, 13);
            this.lblEnd.TabIndex = 13;
            this.lblEnd.Text = "End date:";
            // 
            // lblStart
            // 
            this.lblStart.AccessibleName = "Start date:";
            this.lblStart.Location = new System.Drawing.Point(198, 15);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(53, 13);
            this.lblStart.TabIndex = 14;
            this.lblStart.Text = "Start date:";
            // 
            // cbTimeScale
            // 
            this.cbTimeScale.EditValue = "";
            this.cbTimeScale.Location = new System.Drawing.Point(95, 63);
            this.cbTimeScale.Name = "cbTimeScale";
            this.cbTimeScale.Properties.AccessibleName = "Duration:";
            this.cbTimeScale.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTimeScale.Properties.Items.AddRange(new object[] {
            System.TimeSpan.Parse("00:05:00"),
            System.TimeSpan.Parse("00:10:00"),
            System.TimeSpan.Parse("00:15:00"),
            System.TimeSpan.Parse("00:30:00"),
            System.TimeSpan.Parse("01:00:00"),
            System.TimeSpan.Parse("02:00:00"),
            System.TimeSpan.Parse("03:00:00"),
            System.TimeSpan.Parse("04:00:00"),
            System.TimeSpan.Parse("05:00:00"),
            System.TimeSpan.Parse("06:00:00"),
            System.TimeSpan.Parse("07:00:00"),
            System.TimeSpan.Parse("08:00:00"),
            System.TimeSpan.Parse("09:00:00"),
            System.TimeSpan.Parse("10:00:00"),
            System.TimeSpan.Parse("11:00:00"),
            System.TimeSpan.Parse("12:00:00")});
            this.cbTimeScale.Size = new System.Drawing.Size(85, 20);
            this.cbTimeScale.TabIndex = 22;
            this.cbTimeScale.SelectedIndexChanged += new System.EventHandler(this.cbDuration_SelectedIndexChanged);
            // 
            // lblTimeScale
            // 
            this.lblTimeScale.AccessibleName = "Time Scale:";
            this.lblTimeScale.Location = new System.Drawing.Point(9, 67);
            this.lblTimeScale.Name = "lblTimeScale";
            this.lblTimeScale.Size = new System.Drawing.Size(54, 13);
            this.lblTimeScale.TabIndex = 21;
            this.lblTimeScale.Text = "Time S&cale:";
            // 
            // steToTime
            // 
            this.steToTime.EditValue = new System.DateTime(2005, 8, 24, 0, 0, 0, 0);
            this.steToTime.Location = new System.Drawing.Point(416, 37);
            this.steToTime.Name = "steToTime";
            this.steToTime.Properties.AccessibleName = "Print to:";
            this.steToTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.steToTime.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.steToTime.Size = new System.Drawing.Size(77, 20);
            this.steToTime.TabIndex = 20;
            // 
            // steFromTime
            // 
            this.steFromTime.EditValue = new System.DateTime(2005, 8, 24, 0, 0, 0, 0);
            this.steFromTime.Location = new System.Drawing.Point(416, 11);
            this.steFromTime.Name = "steFromTime";
            this.steFromTime.Properties.AccessibleName = "Print from:";
            this.steFromTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.steFromTime.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.steFromTime.Size = new System.Drawing.Size(77, 20);
            this.steFromTime.TabIndex = 18;
            // 
            // lblPrintTo
            // 
            this.lblPrintTo.AccessibleName = "Print to:";
            this.lblPrintTo.Location = new System.Drawing.Point(360, 41);
            this.lblPrintTo.Name = "lblPrintTo";
            this.lblPrintTo.Size = new System.Drawing.Size(39, 13);
            this.lblPrintTo.TabIndex = 19;
            this.lblPrintTo.Text = "Print &to:";
            // 
            // lblPrintFrom
            // 
            this.lblPrintFrom.AccessibleName = "Print from:";
            this.lblPrintFrom.Location = new System.Drawing.Point(360, 15);
            this.lblPrintFrom.Name = "lblPrintFrom";
            this.lblPrintFrom.Size = new System.Drawing.Size(51, 13);
            this.lblPrintFrom.TabIndex = 17;
            this.lblPrintFrom.Text = "Print fro&m:";
            // 
            // lbResourceCount
            // 
            this.lbResourceCount.Location = new System.Drawing.Point(9, 15);
            this.lbResourceCount.Name = "lbResourceCount";
            this.lbResourceCount.Size = new System.Drawing.Size(81, 13);
            this.lbResourceCount.TabIndex = 39;
            this.lbResourceCount.Text = "Resource Count:";
            // 
            // spinResourceCount
            // 
            this.spinResourceCount.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinResourceCount.Location = new System.Drawing.Point(95, 11);
            this.spinResourceCount.Name = "spinResourceCount";
            this.spinResourceCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinResourceCount.Properties.IsFloatValue = false;
            this.spinResourceCount.Properties.Mask.EditMask = "N00";
            this.spinResourceCount.Properties.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spinResourceCount.Size = new System.Drawing.Size(85, 20);
            this.spinResourceCount.TabIndex = 38;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(9, 41);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(55, 13);
            this.labelControl1.TabIndex = 39;
            this.labelControl1.Text = "Day Count:";
            // 
            // spinDayCount
            // 
            this.spinDayCount.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinDayCount.Location = new System.Drawing.Point(95, 37);
            this.spinDayCount.Name = "spinDayCount";
            this.spinDayCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinDayCount.Properties.IsFloatValue = false;
            this.spinDayCount.Properties.Mask.EditMask = "N00";
            this.spinDayCount.Properties.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spinDayCount.Size = new System.Drawing.Size(85, 20);
            this.spinDayCount.TabIndex = 38;
            // 
            // PreviewControl
            // 
            this.Name = "PreviewControl";
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            this.pnlSettings.ResumeLayout(false);
            this.pnlSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintAllAppointments.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEnd.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStart.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTimeScale.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.steToTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.steFromTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinResourceCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinDayCount.Properties)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
                
        protected internal virtual void UnsubscribeEvents() {
            this.spinDayCount.EditValueChanged -= new EventHandler(spinDayCount_EditValueChanged);
            this.spinResourceCount.EditValueChanged -= new EventHandler(spinResourceCount_EditValueChanged);
            this.edtStart.EditValueChanged -= new EventHandler(StartEditValueChanged);
            this.edtEnd.EditValueChanged -= new EventHandler(EndEditValueChanged);
            this.steFromTime.EditValueChanged -= new System.EventHandler(steFromTime_EditValueChanged);
            this.steToTime.EditValueChanged -= new System.EventHandler(steToTime_EditValueChanged);
        }
        
        protected internal virtual void SubscribeEvents() {
            this.spinDayCount.EditValueChanged += new EventHandler(spinDayCount_EditValueChanged);
            this.spinResourceCount.EditValueChanged += new EventHandler(spinResourceCount_EditValueChanged);
            this.edtStart.EditValueChanged += new EventHandler(StartEditValueChanged);
            this.edtEnd.EditValueChanged += new EventHandler(EndEditValueChanged);
            this.steFromTime.EditValueChanged += new System.EventHandler(steFromTime_EditValueChanged);
            this.steToTime.EditValueChanged += new System.EventHandler(steToTime_EditValueChanged);
        }

        void spinResourceCount_EditValueChanged(object sender, EventArgs e) {
            this.visibleResourceCount = Convert.ToInt32(spinResourceCount.EditValue);
            UpdateActiveReport();
        }
        void spinDayCount_EditValueChanged(object sender, EventArgs e) {
            this.visibleDayCount = Convert.ToInt32(spinDayCount.EditValue);
            UpdateActiveReport();
        }
        void cbDuration_SelectedIndexChanged(object sender, EventArgs e) {
            this.timeScale = cbTimeScale.Duration;
            UpdateActiveReport();
        }
        void StartEditValueChanged(object sender, EventArgs e) {
            UnsubscribeEvents();
            if (!IsValidInterval(StartDate, EndDate))
                edtEnd.EditValue = StartDate;

            this.printInterval = new TimeInterval(StartDate, EndDate);
            SubscribeEvents();
            UpdateActiveReport();
        }
        protected internal virtual bool IsValidInterval(DateTime start, DateTime end) {
            return start <= end;
        }
        void EndEditValueChanged(object sender, EventArgs e) {
            UnsubscribeEvents();
            if (!IsValidInterval(StartDate, EndDate))
                edtStart.EditValue = EndDate.AddDays(-1);

            this.printInterval = new TimeInterval(StartDate, EndDate);
            SubscribeEvents();
            UpdateActiveReport();
        }

        void steFromTime_EditValueChanged(object sender, EventArgs e) {
            UpdateVisibleTime();
        }
        void steToTime_EditValueChanged(object sender, EventArgs e) {
            UpdateVisibleTime();
        }
        protected internal virtual void UpdateVisibleTime() {
            //Debug.Assert(edtStart.Time.Ticks < DateTimeHelper.DaySpan.Ticks);
            //Debug.Assert(edtEnd.Time.Ticks < DateTimeHelper.DaySpan.Ticks);
            TimeSpan start = steFromTime.Time.TimeOfDay;
            TimeSpan end = steToTime.Time.TimeOfDay;
            TimeSpan duration = end - start;
            if (duration.Ticks <= 0)
                end += DateTimeHelper.DaySpan;
            
            this.visibleTime = new TimeOfDayInterval(start, end);
            UpdateActiveReport();
        }

        private void chkPrintAllAppointments_CheckedChanged(object sender, EventArgs e) {
            printAllAppointments = chkPrintAllAppointments.Checked;
            UpdateActiveReport();
        }
	}
}

