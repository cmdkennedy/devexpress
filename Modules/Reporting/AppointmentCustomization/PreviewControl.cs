using System;
using DevExpress.XtraScheduler.Reporting;
using DevExpress.XtraEditors;

namespace DevExpress.XtraScheduler.Demos.Reporting.AppointmentCustomization {

	public class PreviewControl : SingleReportPreviewControl {
        private System.ComponentModel.IContainer components = null;

        //bool showReminder;
        //bool showReccurence;
        
        bool allowInitAptText;
        bool allowInitAptImages;
        private CheckEdit chkAptImages;
        private CheckEdit chkAptText;
        private GroupControl groupControl1;
        private CheckEdit chkShowRecurrence;
        private ImageComboBoxEdit cbStatus;
        private LabelControl lblStatus;
        private ImageComboBoxEdit cbTimeDisplayType;
        private LabelControl lblTime;
        private CheckEdit chkShowReminder;
        private CheckEdit chkStartTimeVisibility;
        private CheckEdit chkEndTimeVisibility;

        bool showStartTime;
        bool showEndTime;
        AppointmentStatusDisplayType statusDisplayType;
        AppointmentTimeDisplayType timeDisplayType;

        bool showBell;
        bool showRecurrence;

		public PreviewControl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();			
			InitializeControlValues();            
		}

		public new Report Report { get { return (Report)base.Report; } }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		protected override XtraSchedulerReport CreateReportInstance() {
			return new Report();
		}
        protected override void InitAdapterTimeInterval() {
            StoragePrintAdapter.TimeInterval = new TimeInterval(DemoUtils.Date, TimeSpan.FromDays(7));
        }
		void InitializeControlValues() {
            allowInitAptText = chkAptText.Checked;
            allowInitAptImages = chkAptImages.Checked;
            
            cbTimeDisplayType.SelectedIndex = 0;
            cbStatus.SelectedIndex = 0;
            showStartTime = chkStartTimeVisibility.Checked;
            showEndTime = chkEndTimeVisibility.Checked;
            showRecurrence = chkShowRecurrence.Checked;
            showBell = chkShowReminder.Checked;
		}
		protected override void UpdateReportProperties(XtraSchedulerReport report) {
			base.UpdateReportProperties(report);
            Report.AllowInitAppointmentText = allowInitAptText;
            Report.AllowInitAppointmentImages = allowInitAptImages;

            Report.ShowStartTime = showStartTime;
            Report.ShowEndTime = showEndTime;
            Report.StatusDisplayType = statusDisplayType;
            Report.TimeDisplayType = timeDisplayType;
            Report.ShowBell = showBell;
            Report.ShowRecurrence = showRecurrence;
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.chkAptImages = new DevExpress.XtraEditors.CheckEdit();
            this.chkAptText = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.chkShowRecurrence = new DevExpress.XtraEditors.CheckEdit();
            this.cbStatus = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.lblStatus = new DevExpress.XtraEditors.LabelControl();
            this.cbTimeDisplayType = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.lblTime = new DevExpress.XtraEditors.LabelControl();
            this.chkShowReminder = new DevExpress.XtraEditors.CheckEdit();
            this.chkStartTimeVisibility = new DevExpress.XtraEditors.CheckEdit();
            this.chkEndTimeVisibility = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).BeginInit();

            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            this.pnlSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAptImages.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAptText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowRecurrence.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTimeDisplayType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowReminder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStartTimeVisibility.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEndTimeVisibility.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // smplEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(616, 67);
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.groupControl1);
            this.pnlSettings.Controls.Add(this.chkAptImages);
            this.pnlSettings.Controls.Add(this.chkAptText);
            this.pnlSettings.Size = new System.Drawing.Size(700, 96);
            this.pnlSettings.Controls.SetChildIndex(this.btnEdit, 0);
            this.pnlSettings.Controls.SetChildIndex(this.chkAptText, 0);
            this.pnlSettings.Controls.SetChildIndex(this.chkAptImages, 0);
            this.pnlSettings.Controls.SetChildIndex(this.groupControl1, 0);
            // 
            // panelSeparatorControl
            // 
            this.panelSeparatorControl.Location = new System.Drawing.Point(0, 96);
            // 
            // printControl
            // 
            this.printControl.Location = new System.Drawing.Point(0, 104);
            this.printControl.Size = new System.Drawing.Size(700, 292);
            // 
            // chkAptImages
            // 
            this.chkAptImages.EditValue = true;
            this.chkAptImages.Location = new System.Drawing.Point(389, 61);
            this.chkAptImages.Name = "chkAptImages";
            this.chkAptImages.Properties.Caption = "Custom Images";
            this.chkAptImages.Size = new System.Drawing.Size(103, 19);
            this.chkAptImages.TabIndex = 49;
            this.chkAptImages.CheckedChanged += new System.EventHandler(this.chkAptImages_CheckedChanged);
            // 
            // chkAptText
            // 
            this.chkAptText.EditValue = true;
            this.chkAptText.Location = new System.Drawing.Point(389, 38);
            this.chkAptText.Name = "chkAptText";
            this.chkAptText.Properties.Caption = "Custom Text";
            this.chkAptText.Size = new System.Drawing.Size(96, 19);
            this.chkAptText.TabIndex = 48;
            this.chkAptText.CheckedChanged += new System.EventHandler(this.chkAptText_CheckedChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.chkShowRecurrence);
            this.groupControl1.Controls.Add(this.cbStatus);
            this.groupControl1.Controls.Add(this.lblStatus);
            this.groupControl1.Controls.Add(this.cbTimeDisplayType);
            this.groupControl1.Controls.Add(this.lblTime);
            this.groupControl1.Controls.Add(this.chkShowReminder);
            this.groupControl1.Controls.Add(this.chkStartTimeVisibility);
            this.groupControl1.Controls.Add(this.chkEndTimeVisibility);
            this.groupControl1.Location = new System.Drawing.Point(8, 8);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(359, 80);
            this.groupControl1.TabIndex = 51;
            this.groupControl1.Text = "Display Options";
            // 
            // chkShowRecurrence
            // 
            this.chkShowRecurrence.EditValue = true;
            this.chkShowRecurrence.Location = new System.Drawing.Point(269, 30);
            this.chkShowRecurrence.Name = "chkShowRecurrence";
            this.chkShowRecurrence.Properties.Caption = "Recurrence";
            this.chkShowRecurrence.Size = new System.Drawing.Size(85, 19);
            this.chkShowRecurrence.TabIndex = 74;
            this.chkShowRecurrence.CheckedChanged += new System.EventHandler(this.chkShowRecurrence_CheckedChanged);
            // 
            // cbStatus
            // 
            this.cbStatus.EditValue = "";
            this.cbStatus.Location = new System.Drawing.Point(53, 29);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbStatus.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Never", DevExpress.XtraScheduler.AppointmentStatusDisplayType.Never, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Time", DevExpress.XtraScheduler.AppointmentStatusDisplayType.Time, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Bounds", DevExpress.XtraScheduler.AppointmentStatusDisplayType.Bounds, -1)});
            this.cbStatus.Size = new System.Drawing.Size(96, 20);
            this.cbStatus.TabIndex = 73;
            this.cbStatus.SelectedIndexChanged += new System.EventHandler(this.cbStatus_SelectedIndexChanged);
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(12, 32);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(35, 13);
            this.lblStatus.TabIndex = 72;
            this.lblStatus.Text = "Status:";
            // 
            // cbTimeDisplayType
            // 
            this.cbTimeDisplayType.EditValue = "";
            this.cbTimeDisplayType.Location = new System.Drawing.Point(53, 53);
            this.cbTimeDisplayType.Name = "cbTimeDisplayType";
            this.cbTimeDisplayType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTimeDisplayType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Auto", DevExpress.XtraScheduler.AppointmentTimeDisplayType.Auto, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Clock", DevExpress.XtraScheduler.AppointmentTimeDisplayType.Clock, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Text", DevExpress.XtraScheduler.AppointmentTimeDisplayType.Text, -1)});
            this.cbTimeDisplayType.Size = new System.Drawing.Size(96, 20);
            this.cbTimeDisplayType.TabIndex = 22;
            this.cbTimeDisplayType.SelectedIndexChanged += new System.EventHandler(this.cbTimeDisplayType_SelectedIndexChanged);
            // 
            // lblTime
            // 
            this.lblTime.Location = new System.Drawing.Point(12, 55);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(26, 13);
            this.lblTime.TabIndex = 21;
            this.lblTime.Text = "Time:";
            // 
            // chkShowReminder
            // 
            this.chkShowReminder.EditValue = true;
            this.chkShowReminder.Location = new System.Drawing.Point(269, 54);
            this.chkShowReminder.Name = "chkShowReminder";
            this.chkShowReminder.Properties.Caption = "Reminder";
            this.chkShowReminder.Size = new System.Drawing.Size(72, 19);
            this.chkShowReminder.TabIndex = 13;
            this.chkShowReminder.CheckedChanged += new System.EventHandler(this.chkShowReminder_CheckedChanged);
            // 
            // chkStartTimeVisibility
            // 
            this.chkStartTimeVisibility.EditValue = true;
            this.chkStartTimeVisibility.Location = new System.Drawing.Point(164, 29);
            this.chkStartTimeVisibility.Name = "chkStartTimeVisibility";
            this.chkStartTimeVisibility.Properties.Caption = "Start Time";
            this.chkStartTimeVisibility.Size = new System.Drawing.Size(72, 19);
            this.chkStartTimeVisibility.TabIndex = 11;
            this.chkStartTimeVisibility.CheckedChanged += new System.EventHandler(this.chkStartTimeVisibility_CheckedChanged);
            // 
            // chkEndTimeVisibility
            // 
            this.chkEndTimeVisibility.EditValue = true;
            this.chkEndTimeVisibility.Location = new System.Drawing.Point(164, 54);
            this.chkEndTimeVisibility.Name = "chkEndTimeVisibility";
            this.chkEndTimeVisibility.Properties.Caption = "End Time";
            this.chkEndTimeVisibility.Size = new System.Drawing.Size(72, 19);
            this.chkEndTimeVisibility.TabIndex = 10;
            this.chkEndTimeVisibility.CheckedChanged += new System.EventHandler(this.chkEndTimeVisibility_CheckedChanged);
            // 
            // PreviewControl
            // 
            this.Name = "PreviewControl";
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).EndInit();

            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            this.pnlSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAptImages.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAptText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowRecurrence.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTimeDisplayType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowReminder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStartTimeVisibility.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkEndTimeVisibility.Properties)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

        private void chkAptText_CheckedChanged(object sender, EventArgs e) {
            allowInitAptText = ((CheckEdit)sender).Checked;
            UpdateActiveReport();
        }

        private void chkAptImages_CheckedChanged(object sender, EventArgs e) {
            allowInitAptImages = ((CheckEdit)sender).Checked;
            UpdateActiveReport();
        }

        private void chkStartTimeVisibility_CheckedChanged(object sender, EventArgs e) {
            showStartTime = ((CheckEdit)sender).Checked;
            UpdateActiveReport();
        }

        private void chkEndTimeVisibility_CheckedChanged(object sender, EventArgs e) {
            showEndTime = ((CheckEdit)sender).Checked;
            UpdateActiveReport();

        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e) {
            statusDisplayType = (AppointmentStatusDisplayType)cbStatus.EditValue;
            UpdateActiveReport();
        }

        private void cbTimeDisplayType_SelectedIndexChanged(object sender, EventArgs e) {
            timeDisplayType = (AppointmentTimeDisplayType)cbTimeDisplayType.EditValue;
            UpdateActiveReport();
        }

        private void chkShowRecurrence_CheckedChanged(object sender, EventArgs e) {
            showRecurrence = ((CheckEdit)sender).Checked;
            UpdateActiveReport();
        }

        private void chkShowReminder_CheckedChanged(object sender, EventArgs e) {
            showBell = ((CheckEdit)sender).Checked;
            UpdateActiveReport();
        }
	}
}

