using System;
using DevExpress.XtraScheduler.Reporting;

namespace DevExpress.XtraScheduler.Demos.Reporting.TimelineStyle {

	public class PreviewControl : SingleReportPreviewControl {
		private System.ComponentModel.IContainer components = null;
		private DevExpress.XtraEditors.LabelControl lbTimeCells;
		private DevExpress.XtraEditors.ComboBoxEdit cmbFirstTimeScale;
        private DevExpress.XtraEditors.LabelControl lbResourceCount;
        private DevExpress.XtraEditors.SpinEdit spinResourceCount;
        protected DevExpress.XtraEditors.DateEdit edtEnd;
        protected DevExpress.XtraEditors.DateEdit edtStart;
        protected DevExpress.XtraEditors.LabelControl lblEnd;
        protected DevExpress.XtraEditors.LabelControl lblStart;

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SpinEdit spinIntervalCount;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit cmbSecondTimeScale;

        TimeInterval printInterval = TimeInterval.Empty;
        int visibleResourceCount;
        int visibleIntevalCount;
        string firstLevelTimeScale;
        string secondLevelTimeScale;        

		public new Report Report { get { return (Report)base.Report; } }

		public PreviewControl()
		{
			// This call is required by the Windows Form Designer. 
			InitializeComponent();
            PopulateFirstLevelScaleCombo();
            PopulateSecondLevelScaleCombo();
			InitializeControlValues();
            SubscribeEvents();
		}
        private DateTime EndDate { get { return edtEnd.DateTime.AddDays(1); } set { edtEnd.DateTime = value.AddDays(-1); } }
        private DateTime StartDate { get { return edtStart.DateTime; } set { edtStart.DateTime = value; } }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		protected override XtraSchedulerReport CreateReportInstance() {
			return new Report();
		}

		void InitializeControlValues() {
			visibleIntevalCount = Report.VisibleIntervalCount;
            visibleResourceCount = Report.VisibleResourceCount;

            firstLevelTimeScale = Report.FirstLevelTimeScale;
            secondLevelTimeScale = Report.SecondLevelTimeScale;
            cmbFirstTimeScale.EditValue = firstLevelTimeScale;
            cmbSecondTimeScale.EditValue = secondLevelTimeScale;

            spinIntervalCount.EditValue = visibleIntevalCount;
            spinResourceCount.EditValue = visibleResourceCount;

            printInterval = new TimeInterval(DemoUtils.Date.AddDays(-30), DemoUtils.Date.AddDays(30));
            StartDate = printInterval.Start;
            EndDate = printInterval.End;
		}
		protected override void UpdateReportProperties(XtraSchedulerReport report) {
            StoragePrintAdapter.TimeInterval = printInterval;

            base.UpdateReportProperties(report);
            Report.VisibleResourceCount = visibleResourceCount;
            Report.VisibleIntervalCount = visibleIntevalCount;
            Report.FirstLevelTimeScale = firstLevelTimeScale;
            Report.SecondLevelTimeScale = secondLevelTimeScale;
		}
        private void PopulateFirstLevelScaleCombo() {
            cmbFirstTimeScale.Properties.Items.Clear();
            int scaleCount = Report.Scales.Count;
            for (int i = 0; i < scaleCount - 1; i++) {
                cmbFirstTimeScale.Properties.Items.Add(Report.Scales[i].DisplayName);
            }
            cmbFirstTimeScale.SelectedIndex = 0;
            firstLevelTimeScale = (string)cmbFirstTimeScale.SelectedItem;
        }
        void PopulateSecondLevelScaleCombo() {
            int index = cmbFirstTimeScale.SelectedIndex;

            cmbSecondTimeScale.Properties.Items.Clear();
            for (int i = index + 1; i < Report.Scales.Count; i++) {
                cmbSecondTimeScale.Properties.Items.Add(Report.Scales[i].DisplayName);
            }
            cmbSecondTimeScale.SelectedIndex = 0;
            secondLevelTimeScale = (string)cmbSecondTimeScale.SelectedItem;
        }

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreviewControl));
            this.lbTimeCells = new DevExpress.XtraEditors.LabelControl();
            this.cmbFirstTimeScale = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbResourceCount = new DevExpress.XtraEditors.LabelControl();
            this.spinResourceCount = new DevExpress.XtraEditors.SpinEdit();
            this.edtEnd = new DevExpress.XtraEditors.DateEdit();
            this.edtStart = new DevExpress.XtraEditors.DateEdit();
            this.lblEnd = new DevExpress.XtraEditors.LabelControl();
            this.lblStart = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.spinIntervalCount = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cmbSecondTimeScale = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fPrintBarManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            this.pnlSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFirstTimeScale.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinResourceCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEnd.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStart.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinIntervalCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSecondTimeScale.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // smplEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(620, 35);
            // 
            // fPrintBarManager
            // 
            this.fPrintBarManager.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("fPrintBarManager.ImageStream")));
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.labelControl2);
            this.pnlSettings.Controls.Add(this.cmbSecondTimeScale);
            this.pnlSettings.Controls.Add(this.labelControl1);
            this.pnlSettings.Controls.Add(this.spinIntervalCount);
            this.pnlSettings.Controls.Add(this.lbResourceCount);
            this.pnlSettings.Controls.Add(this.spinResourceCount);
            this.pnlSettings.Controls.Add(this.edtEnd);
            this.pnlSettings.Controls.Add(this.edtStart);
            this.pnlSettings.Controls.Add(this.lblEnd);
            this.pnlSettings.Controls.Add(this.lblStart);
            this.pnlSettings.Controls.Add(this.lbTimeCells);
            this.pnlSettings.Controls.Add(this.cmbFirstTimeScale);
            this.pnlSettings.Size = new System.Drawing.Size(700, 68);
            this.pnlSettings.Controls.SetChildIndex(this.cmbFirstTimeScale, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lbTimeCells, 0);
            this.pnlSettings.Controls.SetChildIndex(this.btnEdit, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lblStart, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lblEnd, 0);
            this.pnlSettings.Controls.SetChildIndex(this.edtStart, 0);
            this.pnlSettings.Controls.SetChildIndex(this.edtEnd, 0);
            this.pnlSettings.Controls.SetChildIndex(this.spinResourceCount, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lbResourceCount, 0);
            this.pnlSettings.Controls.SetChildIndex(this.spinIntervalCount, 0);
            this.pnlSettings.Controls.SetChildIndex(this.labelControl1, 0);
            this.pnlSettings.Controls.SetChildIndex(this.cmbSecondTimeScale, 0);
            this.pnlSettings.Controls.SetChildIndex(this.labelControl2, 0);
            // 
            // panelSeparatorControl
            // 
            this.panelSeparatorControl.Location = new System.Drawing.Point(0, 68);
            // 
            // printControl
            // 
            this.printControl.Location = new System.Drawing.Point(0, 76);
            this.printControl.Size = new System.Drawing.Size(700, 320);
            // 
            // lbTimeCells
            // 
            this.lbTimeCells.Location = new System.Drawing.Point(360, 15);
            this.lbTimeCells.Name = "lbTimeCells";
            this.lbTimeCells.Size = new System.Drawing.Size(77, 13);
            this.lbTimeCells.TabIndex = 4;
            this.lbTimeCells.Text = "First level scale:";
            // 
            // cmbFirstTimeScale
            // 
            this.cmbFirstTimeScale.EditValue = "";
            this.cmbFirstTimeScale.Location = new System.Drawing.Point(456, 11);
            this.cmbFirstTimeScale.Name = "cmbFirstTimeScale";
            this.cmbFirstTimeScale.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbFirstTimeScale.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbFirstTimeScale.Size = new System.Drawing.Size(84, 20);
            this.cmbFirstTimeScale.TabIndex = 5;
            // 
            // lbResourceCount
            // 
            this.lbResourceCount.Location = new System.Drawing.Point(9, 15);
            this.lbResourceCount.Name = "lbResourceCount";
            this.lbResourceCount.Size = new System.Drawing.Size(81, 13);
            this.lbResourceCount.TabIndex = 47;
            this.lbResourceCount.Text = "Resource Count:";
            // 
            // spinResourceCount
            // 
            this.spinResourceCount.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinResourceCount.Location = new System.Drawing.Point(95, 11);
            this.spinResourceCount.Name = "spinResourceCount";
            this.spinResourceCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinResourceCount.Properties.IsFloatValue = false;
            this.spinResourceCount.Properties.Mask.EditMask = "N00";
            this.spinResourceCount.Properties.MaxValue = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.spinResourceCount.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinResourceCount.Size = new System.Drawing.Size(85, 20);
            this.spinResourceCount.TabIndex = 46;
            this.spinResourceCount.EditValueChanged += new System.EventHandler(this.spinResourceCount_EditValueChanged);
            // 
            // edtEnd
            // 
            this.edtEnd.EditValue = new System.DateTime(2005, 9, 5, 0, 0, 0, 0);
            this.edtEnd.Location = new System.Drawing.Point(256, 37);
            this.edtEnd.Name = "edtEnd";
            this.edtEnd.Properties.AccessibleName = "End date:";
            this.edtEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.edtEnd.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.edtEnd.Size = new System.Drawing.Size(86, 20);
            this.edtEnd.TabIndex = 43;
            // 
            // edtStart
            // 
            this.edtStart.EditValue = new System.DateTime(2005, 9, 5, 0, 0, 0, 0);
            this.edtStart.Location = new System.Drawing.Point(256, 11);
            this.edtStart.Name = "edtStart";
            this.edtStart.Properties.AccessibleName = "Start date:";
            this.edtStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.edtStart.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.edtStart.Size = new System.Drawing.Size(86, 20);
            this.edtStart.TabIndex = 42;
            // 
            // lblEnd
            // 
            this.lblEnd.AccessibleName = "End date:";
            this.lblEnd.Location = new System.Drawing.Point(198, 41);
            this.lblEnd.Name = "lblEnd";
            this.lblEnd.Size = new System.Drawing.Size(47, 13);
            this.lblEnd.TabIndex = 40;
            this.lblEnd.Text = "End date:";
            // 
            // lblStart
            // 
            this.lblStart.AccessibleName = "Start date:";
            this.lblStart.Location = new System.Drawing.Point(198, 15);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(53, 13);
            this.lblStart.TabIndex = 41;
            this.lblStart.Text = "Start date:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(9, 41);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(74, 13);
            this.labelControl1.TabIndex = 49;
            this.labelControl1.Text = "Interval Count:";
            // 
            // spinIntervalCount
            // 
            this.spinIntervalCount.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinIntervalCount.Location = new System.Drawing.Point(95, 37);
            this.spinIntervalCount.Name = "spinIntervalCount";
            this.spinIntervalCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinIntervalCount.Properties.IsFloatValue = false;
            this.spinIntervalCount.Properties.Mask.EditMask = "N00";
            this.spinIntervalCount.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinIntervalCount.Size = new System.Drawing.Size(85, 20);
            this.spinIntervalCount.TabIndex = 48;
            this.spinIntervalCount.EditValueChanged += new System.EventHandler(this.spinIntervalCount_EditValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(360, 41);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(91, 13);
            this.labelControl2.TabIndex = 50;
            this.labelControl2.Text = "Second level scale:";
            // 
            // cmbSecondTimeScale
            // 
            this.cmbSecondTimeScale.EditValue = "";
            this.cmbSecondTimeScale.Location = new System.Drawing.Point(456, 37);
            this.cmbSecondTimeScale.Name = "cmbSecondTimeScale";
            this.cmbSecondTimeScale.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSecondTimeScale.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbSecondTimeScale.Size = new System.Drawing.Size(84, 20);
            this.cmbSecondTimeScale.TabIndex = 51;
            // 
            // PreviewControl
            // 
            this.Name = "PreviewControl";
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fPrintBarManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            this.pnlSettings.ResumeLayout(false);
            this.pnlSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFirstTimeScale.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinResourceCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEnd.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStart.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinIntervalCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSecondTimeScale.Properties)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private void cmbFirstTimeScale_SelectedIndexChanged(object sender, EventArgs e) {
            firstLevelTimeScale = cmbFirstTimeScale.EditValue.ToString();

            PopulateSecondLevelScaleCombo();
			UpdateActiveReport();
		}
        private void cmbSecondTimeScale_SelectedIndexChanged(object sender, EventArgs e) {
            secondLevelTimeScale = cmbSecondTimeScale.EditValue.ToString();
            UpdateActiveReport();
        }
        private void spinResourceCount_EditValueChanged(object sender, EventArgs e) {
            visibleResourceCount = Convert.ToInt32(spinResourceCount.EditValue);
            UpdateActiveReport();
        }

        private void spinIntervalCount_EditValueChanged(object sender, EventArgs e) {
            visibleIntevalCount = Convert.ToInt32(spinIntervalCount.EditValue);
            UpdateActiveReport();
        }
        protected internal virtual void UnsubscribeEvents() {
            edtStart.EditValueChanged -= new EventHandler(StartEditValueChanged);
            edtEnd.EditValueChanged -= new EventHandler(EndEditValueChanged);
            this.cmbFirstTimeScale.SelectedIndexChanged -= new System.EventHandler(this.cmbFirstTimeScale_SelectedIndexChanged);
            this.cmbSecondTimeScale.SelectedIndexChanged -= new System.EventHandler(this.cmbSecondTimeScale_SelectedIndexChanged);

        }
        protected internal virtual void SubscribeEvents() {
            edtStart.EditValueChanged += new EventHandler(StartEditValueChanged);
            edtEnd.EditValueChanged += new EventHandler(EndEditValueChanged);
            this.cmbFirstTimeScale.SelectedIndexChanged += new System.EventHandler(this.cmbFirstTimeScale_SelectedIndexChanged);
            this.cmbSecondTimeScale.SelectedIndexChanged += new System.EventHandler(this.cmbSecondTimeScale_SelectedIndexChanged);
        }
        protected internal virtual bool IsValidInterval(DateTime start, DateTime end) {
            return start <= end;
        }
        private void StartEditValueChanged(object sender, EventArgs e) {
            UnsubscribeEvents();
            if (!IsValidInterval(StartDate, EndDate))
                edtEnd.EditValue = StartDate;

            this.printInterval = new TimeInterval(StartDate, EndDate);
            SubscribeEvents();
            UpdateActiveReport();
        }

        private void EndEditValueChanged(object sender, EventArgs e) {
            UnsubscribeEvents();
            if (!IsValidInterval(StartDate, EndDate))
                edtStart.EditValue = EndDate.AddDays(-1);

            this.printInterval = new TimeInterval(StartDate, EndDate);
            SubscribeEvents();
            UpdateActiveReport();
        }

	}
}

