using System;
using DevExpress.XtraScheduler.Reporting;

namespace DevExpress.XtraScheduler.Demos.Reporting.SmartSyncPrinting {

	public class PreviewControl : SingleReportPreviewControl {
		private System.ComponentModel.IContainer components = null;
		private DevExpress.XtraEditors.LabelControl lbGroupType;
		private DevExpress.XtraEditors.RadioGroup rgrpGroupType;

		SchedulerGroupType groupType;

		public PreviewControl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			InitializeControlValues();            
		}

		public new Report Report { get { return (Report)base.Report; } }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
        protected override void FillReportSourceData() {
            DemoUtils.FillReportsStorageData(SchedulerStorage);
        }
		protected override XtraSchedulerReport CreateReportInstance() {
			return new Report();
		}
        protected override void InitAdapterTimeInterval() {
            StoragePrintAdapter.TimeInterval = new TimeInterval(DemoUtils.Date.AddDays(17), TimeSpan.FromDays(4));
        }
		void InitializeControlValues() {
            groupType = StoragePrintAdapter.SmartSyncOptions.GroupType;
            rgrpGroupType.EditValue = groupType; 
		}
		protected override void UpdateReportProperties(XtraSchedulerReport report) {
			base.UpdateReportProperties(report);
            StoragePrintAdapter.SmartSyncOptions.GroupType = groupType;
		}
		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lbGroupType = new DevExpress.XtraEditors.LabelControl();
            this.rgrpGroupType = new DevExpress.XtraEditors.RadioGroup();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            this.pnlSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgrpGroupType.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.rgrpGroupType);
            this.pnlSettings.Controls.Add(this.lbGroupType);
            this.pnlSettings.Size = new System.Drawing.Size(700, 42);
            this.pnlSettings.Controls.SetChildIndex(this.lbGroupType, 0);
            this.pnlSettings.Controls.SetChildIndex(this.btnEdit, 0);
            this.pnlSettings.Controls.SetChildIndex(this.rgrpGroupType, 0);
            // 
            // panelSeparatorControl
            // 
            this.panelSeparatorControl.Location = new System.Drawing.Point(0, 42);
            // 
            // printControl
            // 
            this.printControl.Location = new System.Drawing.Point(0, 50);
            this.printControl.Size = new System.Drawing.Size(700, 346);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(620, 9);
            // 
            // storagePrintAdapter
            // 
            this.storagePrintAdapter.EnableSmartSync = true;
            // 
            // lbGroupType
            // 
            this.lbGroupType.Location = new System.Drawing.Point(9, 15);
            this.lbGroupType.Name = "lbGroupType";
            this.lbGroupType.Size = new System.Drawing.Size(60, 13);
            this.lbGroupType.TabIndex = 4;
            this.lbGroupType.Text = "Group Type:";
            // 
            // rgrpGroupType
            // 
            this.rgrpGroupType.Location = new System.Drawing.Point(70, 10);
            this.rgrpGroupType.Name = "rgrpGroupType";
            this.rgrpGroupType.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rgrpGroupType.Properties.Appearance.Options.UseBackColor = true;
            this.rgrpGroupType.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rgrpGroupType.Properties.Columns = 5;
            this.rgrpGroupType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(DevExpress.XtraScheduler.SchedulerGroupType.None, "None"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(DevExpress.XtraScheduler.SchedulerGroupType.Date, "Date"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(DevExpress.XtraScheduler.SchedulerGroupType.Resource, "Resource")});
            this.rgrpGroupType.Size = new System.Drawing.Size(218, 24);
            this.rgrpGroupType.TabIndex = 5;
            this.rgrpGroupType.SelectedIndexChanged += new System.EventHandler(this.rgrpGroupType_SelectedIndexChanged);
            // 
            // PreviewControl
            // 
            this.Name = "PreviewControl";
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            this.pnlSettings.ResumeLayout(false);
            this.pnlSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgrpGroupType.Properties)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

        private void rgrpGroupType_SelectedIndexChanged(object sender, EventArgs e) {
            groupType = (SchedulerGroupType)rgrpGroupType.EditValue;
            UpdateActiveReport();
        }

	}
}

