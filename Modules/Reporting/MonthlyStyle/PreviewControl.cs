using System;
using DevExpress.XtraScheduler.Reporting;

namespace DevExpress.XtraScheduler.Demos.Reporting.MonthlyStyle {

	
	public class PreviewControl : SingleReportPreviewControl {
        System.ComponentModel.IContainer components = null;
        DevExpress.XtraEditors.LabelControl lbResourceCount;
        DevExpress.XtraEditors.SpinEdit spinResourceCount;
        protected DevExpress.XtraEditors.CheckEdit chkCompressWeekend;
        protected DevExpress.XtraEditors.CheckEdit chkPrintExactlyOneMonth;
        protected DevExpress.XtraEditors.CheckEdit chkDontPrintWeekends;
        DevExpress.XtraEditors.ComboBoxEdit cmbLayout;
        DevExpress.XtraEditors.LabelControl lbLayout;

		bool initialized;
        bool dontPrintWeekends;
        bool compressWeekend;
        bool printExactlyOneMonth;
        int visibleResourceCount = 0;
        int pageLayout = 1;

		public PreviewControl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();			
			InitializeControlValues();			
            SubscribeEvents();
		}

        public new Report Report { get { return (Report)base.Report; } }

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lbResourceCount = new DevExpress.XtraEditors.LabelControl();
            this.spinResourceCount = new DevExpress.XtraEditors.SpinEdit();
            this.cmbLayout = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lbLayout = new DevExpress.XtraEditors.LabelControl();
            this.chkCompressWeekend = new DevExpress.XtraEditors.CheckEdit();
            this.chkPrintExactlyOneMonth = new DevExpress.XtraEditors.CheckEdit();
            this.chkDontPrintWeekends = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            this.pnlSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinResourceCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbLayout.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCompressWeekend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintExactlyOneMonth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDontPrintWeekends.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // smplEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(620, 35);
            this.btnEdit.TabIndex = 5;
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.chkCompressWeekend);
            this.pnlSettings.Controls.Add(this.chkPrintExactlyOneMonth);
            this.pnlSettings.Controls.Add(this.chkDontPrintWeekends);
            this.pnlSettings.Controls.Add(this.cmbLayout);
            this.pnlSettings.Controls.Add(this.lbLayout);
            this.pnlSettings.Controls.Add(this.lbResourceCount);
            this.pnlSettings.Controls.Add(this.spinResourceCount);
            this.pnlSettings.Size = new System.Drawing.Size(700, 68);
            this.pnlSettings.Controls.SetChildIndex(this.btnEdit, 0);
            this.pnlSettings.Controls.SetChildIndex(this.spinResourceCount, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lbResourceCount, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lbLayout, 0);
            this.pnlSettings.Controls.SetChildIndex(this.cmbLayout, 0);
            this.pnlSettings.Controls.SetChildIndex(this.chkDontPrintWeekends, 0);
            this.pnlSettings.Controls.SetChildIndex(this.chkPrintExactlyOneMonth, 0);
            this.pnlSettings.Controls.SetChildIndex(this.chkCompressWeekend, 0);
            // 
            // panelSeparatorControl
            // 
            this.panelSeparatorControl.Location = new System.Drawing.Point(0, 68);
            // 
            // printControl
            // 
            this.printControl.Location = new System.Drawing.Point(0, 76);
            this.printControl.Size = new System.Drawing.Size(700, 320);
            // 
            // lbResourceCount
            // 
            this.lbResourceCount.Location = new System.Drawing.Point(9, 15);
            this.lbResourceCount.Name = "lbResourceCount";
            this.lbResourceCount.Size = new System.Drawing.Size(81, 13);
            this.lbResourceCount.TabIndex = 43;
            this.lbResourceCount.Text = "Resource Count:";
            // 
            // spinResourceCount
            // 
            this.spinResourceCount.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinResourceCount.Location = new System.Drawing.Point(95, 11);
            this.spinResourceCount.Name = "spinResourceCount";
            this.spinResourceCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinResourceCount.Properties.IsFloatValue = false;
            this.spinResourceCount.Properties.Mask.EditMask = "N00";
            this.spinResourceCount.Properties.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spinResourceCount.Size = new System.Drawing.Size(85, 20);
            this.spinResourceCount.TabIndex = 42;
            // 
            // cmbLayout
            // 
            this.cmbLayout.EditValue = "1 page/week";
            this.cmbLayout.Location = new System.Drawing.Point(95, 37);
            this.cmbLayout.Name = "cmbLayout";
            this.cmbLayout.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbLayout.Properties.Items.AddRange(new object[] {
            "1 page/week",
            "2 pages/week"});
            this.cmbLayout.Size = new System.Drawing.Size(85, 20);
            this.cmbLayout.TabIndex = 45;
            // 
            // lbLayout
            // 
            this.lbLayout.Location = new System.Drawing.Point(9, 41);
            this.lbLayout.Name = "lbLayout";
            this.lbLayout.Size = new System.Drawing.Size(37, 13);
            this.lbLayout.TabIndex = 44;
            this.lbLayout.Text = "Layout:";
            // 
            // chkCompressWeekend
            // 
            this.chkCompressWeekend.Location = new System.Drawing.Point(198, 38);
            this.chkCompressWeekend.Name = "chkCompressWeekend";
            this.chkCompressWeekend.Properties.AutoWidth = true;
            this.chkCompressWeekend.Properties.Caption = "Compress weekend";
            this.chkCompressWeekend.Size = new System.Drawing.Size(116, 19);
            this.chkCompressWeekend.TabIndex = 48;
            // 
            // chkPrintExactlyOneMonth
            // 
            this.chkPrintExactlyOneMonth.Location = new System.Drawing.Point(336, 12);
            this.chkPrintExactlyOneMonth.Name = "chkPrintExactlyOneMonth";
            this.chkPrintExactlyOneMonth.Properties.AutoWidth = true;
            this.chkPrintExactlyOneMonth.Properties.Caption = "Print exactly one month per page";
            this.chkPrintExactlyOneMonth.Size = new System.Drawing.Size(183, 19);
            this.chkPrintExactlyOneMonth.TabIndex = 47;
            // 
            // chkDontPrintWeekends
            // 
            this.chkDontPrintWeekends.Location = new System.Drawing.Point(198, 12);
            this.chkDontPrintWeekends.Name = "chkDontPrintWeekends";
            this.chkDontPrintWeekends.Properties.AutoWidth = true;
            this.chkDontPrintWeekends.Properties.Caption = "Don\'t print &weekends";
            this.chkDontPrintWeekends.Size = new System.Drawing.Size(124, 19);
            this.chkDontPrintWeekends.TabIndex = 46;
            // 
            // PreviewControl
            // 
            this.Name = "PreviewControl";
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            this.pnlSettings.ResumeLayout(false);
            this.pnlSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinResourceCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbLayout.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCompressWeekend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrintExactlyOneMonth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDontPrintWeekends.Properties)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if (disposing) {
                if (components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        void SubscribeEvents() {
            this.spinResourceCount.EditValueChanged += new EventHandler(spinResourceCount_EditValueChanged);
            this.chkPrintExactlyOneMonth.CheckedChanged += new EventHandler(chkPrintExactlyOneMonth_CheckedChanged);
            this.chkDontPrintWeekends.CheckedChanged += new EventHandler(chkDontPrintWeekends_CheckedChanged);
            this.chkCompressWeekend.CheckedChanged += new EventHandler(chkCompressWeekend_CheckedChanged);
            this.cmbLayout.SelectedIndexChanged += new EventHandler(cmbLayout_SelectedIndexChanged);
        }
        protected override XtraSchedulerReport CreateReportInstance() {
            return new Report();
        }
        void InitializeControlValues() {
            this.dontPrintWeekends = Report.DontPrintWeekends;
            this.compressWeekend = Report.CompressWeekend;
            this.visibleResourceCount = Report.VisibleResourceCount;
            this.printExactlyOneMonth = Report.PrintExactlyOneMonth;
            this.pageLayout = Report.ColumnCount - 1;
            this.cmbLayout.SelectedIndex = this.pageLayout;
            this.spinResourceCount.Value = this.visibleResourceCount;
            this.spinResourceCount.Properties.MinValue = 1;
            this.spinResourceCount.Properties.MaxValue = StoragePrintAdapter.ResourceCount;
            this.chkCompressWeekend.Checked = this.compressWeekend;
            this.chkDontPrintWeekends.Checked = this.dontPrintWeekends;
            this.chkPrintExactlyOneMonth.Checked = this.printExactlyOneMonth;
            this.initialized = true;
        }
        protected override void UpdateReportProperties(XtraSchedulerReport report) {
            base.UpdateReportProperties(report);
            Report.DontPrintWeekends = this.dontPrintWeekends;
            Report.VisibleResourceCount = this.visibleResourceCount;
            Report.ColumnCount = this.cmbLayout.SelectedIndex + 1;
            Report.CompressWeekend = this.compressWeekend;
            Report.DontPrintWeekends = this.dontPrintWeekends;
            Report.PrintExactlyOneMonth = this.printExactlyOneMonth;
        }	
        void cmbLayout_SelectedIndexChanged(object sender, EventArgs e) {
            this.pageLayout = this.cmbLayout.SelectedIndex + 1;
            UpdateActiveReport();
        }
		void spinResourceCount_EditValueChanged(object sender, EventArgs e) {
			if (!this.initialized)
				return;
			this.visibleResourceCount = Convert.ToInt32(spinResourceCount.Value);
			UpdateActiveReport();
		}
        void chkCompressWeekend_CheckedChanged(object sender, EventArgs e) {
            if (!initialized)
                return;
            this.compressWeekend = this.chkCompressWeekend.Checked;
            UpdateActiveReport();
        }
        void chkDontPrintWeekends_CheckedChanged(object sender, EventArgs e) {
            if (!initialized)
                return;
            this.dontPrintWeekends = this.chkDontPrintWeekends.Checked;
            UpdateActiveReport();
        }
        void chkPrintExactlyOneMonth_CheckedChanged(object sender, EventArgs e) {
            if (!initialized)
                return;
            this.printExactlyOneMonth = this.chkPrintExactlyOneMonth.Checked;
            UpdateActiveReport();
        }
	}	
}

