using System;
using DevExpress.XtraScheduler.Reporting;
using DevExpress.Utils;

namespace DevExpress.XtraScheduler.Demos.Reporting.Appearance {

	public class PreviewControl : SingleReportPreviewControl {
        private System.ComponentModel.IContainer components = null;
        private DevExpress.XtraEditors.SimpleButton btnViewAppearance;
        private DevExpress.XtraEditors.SimpleButton btnResourceColorSchemas;
        private DevExpress.XtraEditors.GroupControl grPrintColorSchema;
        private DevExpress.XtraEditors.ImageComboBoxEdit cmbAppointmentSchema;
        protected DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ImageComboBoxEdit cmbReportSchema;
        protected DevExpress.XtraEditors.LabelControl lblReportSchema;

		public new Report Report { get { return (Report)base.Report; } }

		public PreviewControl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();			
            InitializeControlValues();            
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		protected override XtraSchedulerReport CreateReportInstance() {
			return new Report();
		}
        protected override void InitAdapterTimeInterval() {
            StoragePrintAdapter.TimeInterval = new TimeInterval(DemoUtils.Date, TimeSpan.FromDays(2));
        }
        private void InitializeControlValues() {
            cmbReportSchema.EditValue = Report.ReportColorSchema;
            cmbAppointmentSchema.EditValue = Report.AppointmentsColorSchema;
        }
        protected override void UpdateReportProperties(XtraSchedulerReport report) {
            base.UpdateReportProperties(report);
            Report.ReportColorSchema = (PrintColorSchema)cmbReportSchema.EditValue;
            Report.AppointmentsColorSchema = (PrintColorSchema)cmbAppointmentSchema.EditValue;
        }

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreviewControl));
            this.btnViewAppearance = new DevExpress.XtraEditors.SimpleButton();
            this.btnResourceColorSchemas = new DevExpress.XtraEditors.SimpleButton();
            this.grPrintColorSchema = new DevExpress.XtraEditors.GroupControl();
            this.cmbAppointmentSchema = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cmbReportSchema = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.lblReportSchema = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fPrintBarManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            this.pnlSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grPrintColorSchema)).BeginInit();
            this.grPrintColorSchema.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAppointmentSchema.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbReportSchema.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // smplEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(620, 33);
            // 
            // fPrintBarManager
            // 
            this.fPrintBarManager.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("fPrintBarManager.ImageStream")));
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.grPrintColorSchema);
            this.pnlSettings.Controls.Add(this.btnResourceColorSchemas);
            this.pnlSettings.Controls.Add(this.btnViewAppearance);
            this.pnlSettings.Size = new System.Drawing.Size(700, 66);
            this.pnlSettings.Controls.SetChildIndex(this.btnEdit, 0);
            this.pnlSettings.Controls.SetChildIndex(this.btnViewAppearance, 0);
            this.pnlSettings.Controls.SetChildIndex(this.btnResourceColorSchemas, 0);
            this.pnlSettings.Controls.SetChildIndex(this.grPrintColorSchema, 0);
            // 
            // panelSeparatorControl
            // 
            this.panelSeparatorControl.Location = new System.Drawing.Point(0, 66);
            // 
            // printControl
            // 
            this.printControl.Location = new System.Drawing.Point(0, 74);
            this.printControl.Size = new System.Drawing.Size(700, 322);
            // 
            // btnViewAppearance
            // 
            this.btnViewAppearance.ImageIndex = 2;
            this.btnViewAppearance.Location = new System.Drawing.Point(16, 5);
            this.btnViewAppearance.Name = "btnViewAppearance";
            this.btnViewAppearance.Size = new System.Drawing.Size(175, 24);
            this.btnViewAppearance.TabIndex = 0;
            this.btnViewAppearance.Text = "&Edit View Appearance...";
            this.btnViewAppearance.Click += new System.EventHandler(this.btnEditAppearance_Click);
            // 
            // btnResourceColorSchemas
            // 
            this.btnResourceColorSchemas.ImageIndex = 2;
            this.btnResourceColorSchemas.Location = new System.Drawing.Point(16, 35);
            this.btnResourceColorSchemas.Name = "btnResourceColorSchemas";
            this.btnResourceColorSchemas.Size = new System.Drawing.Size(175, 24);
            this.btnResourceColorSchemas.TabIndex = 1;
            this.btnResourceColorSchemas.Text = "&Edit Resource Color Schemas...";
            this.btnResourceColorSchemas.Click += new System.EventHandler(this.btnResourceColorSchemas_Click);
            // 
            // grPrintColorSchema
            // 
            this.grPrintColorSchema.Controls.Add(this.cmbAppointmentSchema);
            this.grPrintColorSchema.Controls.Add(this.labelControl1);
            this.grPrintColorSchema.Controls.Add(this.cmbReportSchema);
            this.grPrintColorSchema.Controls.Add(this.lblReportSchema);
            this.grPrintColorSchema.Location = new System.Drawing.Point(209, 5);
            this.grPrintColorSchema.Name = "grPrintColorSchema";
            this.grPrintColorSchema.Size = new System.Drawing.Size(345, 53);
            this.grPrintColorSchema.TabIndex = 2;
            this.grPrintColorSchema.Text = "Print Color Schema";
            // 
            // cmbAppointmentSchema
            // 
            this.cmbAppointmentSchema.EditValue = DevExpress.XtraScheduler.Reporting.PrintColorSchema.Default;
            this.cmbAppointmentSchema.Location = new System.Drawing.Point(232, 25);
            this.cmbAppointmentSchema.Name = "cmbAppointmentSchema";
            this.cmbAppointmentSchema.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbAppointmentSchema.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Default", DevExpress.XtraScheduler.Reporting.PrintColorSchema.Default, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("FullColor", DevExpress.XtraScheduler.Reporting.PrintColorSchema.FullColor, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("GrayScale", DevExpress.XtraScheduler.Reporting.PrintColorSchema.GrayScale, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("BlackAndWhite", DevExpress.XtraScheduler.Reporting.PrintColorSchema.BlackAndWhite, -1)});
            this.cmbAppointmentSchema.Size = new System.Drawing.Size(100, 20);
            this.cmbAppointmentSchema.TabIndex = 1;
            this.cmbAppointmentSchema.SelectedIndexChanged += new System.EventHandler(this.cmbAppointmentSchema_SelectedIndexChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.AccessibleName = "ReportSchema:";
            this.labelControl1.Location = new System.Drawing.Point(160, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(66, 13);
            this.labelControl1.TabIndex = 91;
            this.labelControl1.Text = "Appointments";
            // 
            // cmbReportSchema
            // 
            this.cmbReportSchema.EditValue = DevExpress.XtraScheduler.Reporting.PrintColorSchema.Default;
            this.cmbReportSchema.Location = new System.Drawing.Point(44, 25);
            this.cmbReportSchema.Name = "cmbReportSchema";
            this.cmbReportSchema.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbReportSchema.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Default", DevExpress.XtraScheduler.Reporting.PrintColorSchema.Default, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("FullColor", DevExpress.XtraScheduler.Reporting.PrintColorSchema.FullColor, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("GrayScale", DevExpress.XtraScheduler.Reporting.PrintColorSchema.GrayScale, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("BlackAndWhite", DevExpress.XtraScheduler.Reporting.PrintColorSchema.BlackAndWhite, -1)});
            this.cmbReportSchema.Size = new System.Drawing.Size(100, 20);
            this.cmbReportSchema.TabIndex = 0;
            this.cmbReportSchema.SelectedIndexChanged += new System.EventHandler(this.cmbReportSchema_SelectedIndexChanged);
            // 
            // lblReportSchema
            // 
            this.lblReportSchema.AccessibleName = "ReportSchema:";
            this.lblReportSchema.Location = new System.Drawing.Point(5, 28);
            this.lblReportSchema.Name = "lblReportSchema";
            this.lblReportSchema.Size = new System.Drawing.Size(33, 13);
            this.lblReportSchema.TabIndex = 89;
            this.lblReportSchema.Text = "Report";
            // 
            // PreviewControl
            // 
            this.Name = "PreviewControl";
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fPrintBarManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            this.pnlSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grPrintColorSchema)).EndInit();
            this.grPrintColorSchema.ResumeLayout(false);
            this.grPrintColorSchema.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAppointmentSchema.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbReportSchema.Properties)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

        private void btnEditAppearance_Click(object sender, EventArgs e) {
           Report.Appearances.Changed += new EventHandler(Appearances_Changed);
           try {
               AppearancesEditForm frm = new AppearancesEditForm(Report.Appearances, this.FindForm());
               frm.ShowDialog();
           } finally {
               Report.Appearances.Changed -= new EventHandler(Appearances_Changed);
           }
        }
        void Appearances_Changed(object sender, EventArgs e) {
            UpdateActiveReport();
        }

        private void btnResourceColorSchemas_Click(object sender, EventArgs e) {
            StoragePrintAdapter.ResourceColorsChanged += new EventHandler(StoragePrintAdapter_ResourceColorsChanged);
            try {
                ColorSchemasEditForm frm = new ColorSchemasEditForm(StoragePrintAdapter.ResourceColorSchemas, this.FindForm());
                frm.ShowDialog();
            }
            finally {
                StoragePrintAdapter.ResourceColorsChanged -= new EventHandler(StoragePrintAdapter_ResourceColorsChanged);
            }
        }

        void StoragePrintAdapter_ResourceColorsChanged(object sender, EventArgs e) {
            UpdateActiveReport();
        }
        public void ResourceColorSchemasChanged(object sender, CollectionChangedEventArgs<SchedulerColorSchema> e) {
            UpdateActiveReport();
        }

        private void cmbAppointmentSchema_SelectedIndexChanged(object sender, EventArgs e) {
            UpdateActiveReport();
        }

        private void cmbReportSchema_SelectedIndexChanged(object sender, EventArgs e) {
            UpdateActiveReport();
        }
	}
}

