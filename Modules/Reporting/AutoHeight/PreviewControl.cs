using System;
using DevExpress.XtraScheduler.Reporting;
using System.Windows.Forms;
using System.IO;

namespace DevExpress.XtraScheduler.Demos.Reporting.AutoHeight {

    public class PreviewControl : ReportPreviewControlBase {
        private System.ComponentModel.IContainer components = null;
        private DevExpress.XtraEditors.RadioGroup rgrpView;
        private MonthReport monthReport;
        private DevExpress.XtraEditors.SpinEdit spinHeight;
        private DevExpress.XtraEditors.LabelControl lblHeight;
        protected DevExpress.XtraEditors.CheckEdit chkCanGrow;
        protected DevExpress.XtraEditors.CheckEdit chkCanShrink;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private TimelineReport timelineReport;


		public PreviewControl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
            printControl.Zoom = 0.6f;
            InitializeControlValues();
		}

        MonthReport MonthReport { get { return monthReport; } }
        TimelineReport TimelineReport { get { return timelineReport; } }        
        SchedulerViewType ViewType { get { return (SchedulerViewType)rgrpView.EditValue; } }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing) {
            if (disposing) {
                if (components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        protected override void CreateReports() {
            if (monthReport != null)
                monthReport.Dispose();
            this.monthReport = new MonthReport();
            if (timelineReport != null)
                timelineReport.Dispose();
            this.timelineReport = new TimelineReport();
        }
        protected override XtraSchedulerReport GetActiveReport() {
            if (ViewType == SchedulerViewType.Month)
                return MonthReport;
            else
                return TimelineReport;            
        }
        protected override void FillReportSourceData() {
            DemoUtils.FillReportsStorageData(SchedulerStorage);
        }
        protected override void InitAdapterTimeInterval() {
            StoragePrintAdapter.TimeInterval = new TimeInterval(new DateTime(2010, 07, 01), TimeSpan.FromDays(14));
        }
        void InitializeControlValues() {
            IAutoHeightReport report = (IAutoHeightReport)GetActiveReport();
            chkCanShrink.Checked = report.CellsCanShrink;
            chkCanGrow.Checked = report.CellsCanGrow;
            spinHeight.Value = (decimal)report.CellsHeight;
        }

		protected override void UpdateReportProperties(XtraSchedulerReport report) {
            base.UpdateReportProperties(report);
            IAutoHeightReport autoHeightReport = (IAutoHeightReport)report;            
            autoHeightReport.CellsHeight =  (float)spinHeight.Value;
            autoHeightReport.CellsCanGrow = chkCanGrow.Checked;
            autoHeightReport.CellsCanShrink = chkCanShrink.Checked;            
		}
                
        
		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.rgrpView = new DevExpress.XtraEditors.RadioGroup();
            this.spinHeight = new DevExpress.XtraEditors.SpinEdit();
            this.lblHeight = new DevExpress.XtraEditors.LabelControl();
            this.chkCanShrink = new DevExpress.XtraEditors.CheckEdit();
            this.chkCanGrow = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            this.pnlSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgrpView.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCanShrink.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCanGrow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.groupControl1);
            this.pnlSettings.Controls.Add(this.groupControl2);
            this.pnlSettings.Size = new System.Drawing.Size(700, 83);
            this.pnlSettings.Controls.SetChildIndex(this.groupControl2, 0);
            this.pnlSettings.Controls.SetChildIndex(this.groupControl1, 0);
            this.pnlSettings.Controls.SetChildIndex(this.btnEdit, 0);
            // 
            // panelSeparatorControl
            // 
            this.panelSeparatorControl.Location = new System.Drawing.Point(0, 83);
            // 
            // printControl
            // 
            this.printControl.Location = new System.Drawing.Point(0, 91);
            this.printControl.Size = new System.Drawing.Size(700, 305);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(620, 50);
            // 
            // rgrpView
            // 
            this.rgrpView.EditValue = DevExpress.XtraScheduler.SchedulerViewType.Month;
            this.rgrpView.Location = new System.Drawing.Point(5, 28);
            this.rgrpView.Name = "rgrpView";
            this.rgrpView.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rgrpView.Properties.Appearance.Options.UseBackColor = true;
            this.rgrpView.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rgrpView.Properties.Columns = 5;
            this.rgrpView.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(DevExpress.XtraScheduler.SchedulerViewType.Month, "Month"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(DevExpress.XtraScheduler.SchedulerViewType.Timeline, "Timeline")});
            this.rgrpView.Size = new System.Drawing.Size(143, 24);
            this.rgrpView.TabIndex = 5;
            this.rgrpView.SelectedIndexChanged += new System.EventHandler(this.rgrpView_SelectedIndexChanged);
            // 
            // spinHeight
            // 
            this.spinHeight.EditValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.spinHeight.Location = new System.Drawing.Point(46, 30);
            this.spinHeight.Name = "spinHeight";
            this.spinHeight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinHeight.Properties.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spinHeight.Properties.IsFloatValue = false;
            this.spinHeight.Properties.Mask.EditMask = "N00";
            this.spinHeight.Properties.MaxValue = new decimal(new int[] {
            800,
            0,
            0,
            0});
            this.spinHeight.Properties.MinValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.spinHeight.Size = new System.Drawing.Size(72, 20);
            this.spinHeight.TabIndex = 39;
            this.spinHeight.EditValueChanged += new System.EventHandler(this.spinHeight_EditValueChanged);
            // 
            // lblHeight
            // 
            this.lblHeight.Location = new System.Drawing.Point(5, 33);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(35, 13);
            this.lblHeight.TabIndex = 40;
            this.lblHeight.Text = "Height:";
            // 
            // chkCanShrink
            // 
            this.chkCanShrink.Location = new System.Drawing.Point(148, 31);
            this.chkCanShrink.Name = "chkCanShrink";
            this.chkCanShrink.Properties.AutoWidth = true;
            this.chkCanShrink.Properties.Caption = "Can Shrink";
            this.chkCanShrink.Size = new System.Drawing.Size(74, 19);
            this.chkCanShrink.TabIndex = 50;
            this.chkCanShrink.CheckedChanged += new System.EventHandler(this.chkCanShrink_CheckedChanged);
            // 
            // chkCanGrow
            // 
            this.chkCanGrow.Location = new System.Drawing.Point(242, 31);
            this.chkCanGrow.Name = "chkCanGrow";
            this.chkCanGrow.Properties.AutoWidth = true;
            this.chkCanGrow.Properties.Caption = "Can Grow";
            this.chkCanGrow.Size = new System.Drawing.Size(70, 19);
            this.chkCanGrow.TabIndex = 51;
            this.chkCanGrow.CheckedChanged += new System.EventHandler(this.chkCanGrow_CheckedChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.rgrpView);
            this.groupControl1.Location = new System.Drawing.Point(15, 9);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(173, 60);
            this.groupControl1.TabIndex = 77;
            this.groupControl1.Text = "Report Type";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.spinHeight);
            this.groupControl2.Controls.Add(this.lblHeight);
            this.groupControl2.Controls.Add(this.chkCanGrow);
            this.groupControl2.Controls.Add(this.chkCanShrink);
            this.groupControl2.Location = new System.Drawing.Point(214, 9);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(356, 60);
            this.groupControl2.TabIndex = 78;
            this.groupControl2.Text = "Cells Options";
            // 
            // PreviewControl
            // 
            this.Name = "PreviewControl";
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            this.pnlSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgrpView.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCanShrink.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCanGrow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		private void rgrpView_SelectedIndexChanged(object sender, EventArgs e) {           
			UpdateActiveReport();
		}        

        private void spinHeight_EditValueChanged(object sender, EventArgs e) {
            UpdateActiveReport();
        }       
        private void chkCanShrink_CheckedChanged(object sender, EventArgs e) {
            UpdateActiveReport();
        }

        private void chkCanGrow_CheckedChanged(object sender, EventArgs e) {
            UpdateActiveReport();
        }

        private void chkCompressWeekend_CheckedChanged(object sender, EventArgs e) {
            UpdateActiveReport();
        }       

	}

    public interface IAutoHeightReport {        
        bool CellsCanShrink { get; set;}
        bool CellsCanGrow { get; set;}        
        float CellsHeight { get; set;}

    }
}

