using System;
using DevExpress.XtraScheduler.Reporting;

namespace DevExpress.XtraScheduler.Demos.Reporting.MultiColumn {

	public class PreviewControl : SingleReportPreviewControl {
		private System.ComponentModel.IContainer components = null;
		private DevExpress.XtraEditors.LabelControl lbResourceCount;
		private DevExpress.XtraEditors.SpinEdit spinColumnCount;
        private DevExpress.XtraEditors.RadioGroup rgrpColumnArrangement;
        private DevExpress.XtraEditors.LabelControl labelControl1;

        int visibleWeekDayColumnCount = 0;
        ColumnArrangementMode columnArrangement = ColumnArrangementMode.Ascending;
        
		public new Report Report { get { return (Report)base.Report; } }

		public PreviewControl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();		
			InitializeControlValues();
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		protected override XtraSchedulerReport CreateReportInstance() {
			return new Report();
		}
		void InitializeControlValues() {
            visibleWeekDayColumnCount = Report.VisibleWeekDayColumnCount;
            columnArrangement = Report.ColumnArrangement;

            spinColumnCount.Value = visibleWeekDayColumnCount;
            rgrpColumnArrangement.EditValue = columnArrangement;
		}
		protected override void UpdateReportProperties(XtraSchedulerReport report) {
			base.UpdateReportProperties(report);
            Report.VisibleWeekDayColumnCount = visibleWeekDayColumnCount;
            Report.ColumnArrangement = columnArrangement;
		}
		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreviewControl));
            this.spinColumnCount = new DevExpress.XtraEditors.SpinEdit();
            this.lbResourceCount = new DevExpress.XtraEditors.LabelControl();
            this.rgrpColumnArrangement = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fPrintBarManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            this.pnlSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinColumnCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgrpColumnArrangement.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // smplEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(620, 9);
            // 
            // fPrintBarManager
            // 
            this.fPrintBarManager.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("fPrintBarManager.ImageStream")));
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.rgrpColumnArrangement);
            this.pnlSettings.Controls.Add(this.labelControl1);
            this.pnlSettings.Controls.Add(this.lbResourceCount);
            this.pnlSettings.Controls.Add(this.spinColumnCount);
            this.pnlSettings.Size = new System.Drawing.Size(700, 42);
            this.pnlSettings.Controls.SetChildIndex(this.spinColumnCount, 0);
            this.pnlSettings.Controls.SetChildIndex(this.lbResourceCount, 0);
            this.pnlSettings.Controls.SetChildIndex(this.btnEdit, 0);
            this.pnlSettings.Controls.SetChildIndex(this.labelControl1, 0);
            this.pnlSettings.Controls.SetChildIndex(this.rgrpColumnArrangement, 0);
            // 
            // panelSeparatorControl
            // 
            this.panelSeparatorControl.Location = new System.Drawing.Point(0, 42);
            // 
            // printControl
            // 
            this.printControl.Location = new System.Drawing.Point(0, 50);
            this.printControl.Size = new System.Drawing.Size(700, 346);
            // 
            // spinColumnCount
            // 
            this.spinColumnCount.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinColumnCount.Location = new System.Drawing.Point(139, 11);
            this.spinColumnCount.Name = "spinColumnCount";
            this.spinColumnCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinColumnCount.Properties.IsFloatValue = false;
            this.spinColumnCount.Properties.Mask.EditMask = "N00";
            this.spinColumnCount.Properties.MaxValue = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.spinColumnCount.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinColumnCount.Size = new System.Drawing.Size(69, 20);
            this.spinColumnCount.TabIndex = 3;
            this.spinColumnCount.EditValueChanged += new System.EventHandler(this.spinColumnPerPage_EditValueChanged);
            // 
            // lbResourceCount
            // 
            this.lbResourceCount.Location = new System.Drawing.Point(9, 15);
            this.lbResourceCount.Name = "lbResourceCount";
            this.lbResourceCount.Size = new System.Drawing.Size(125, 13);
            this.lbResourceCount.TabIndex = 4;
            this.lbResourceCount.Text = "WeekDays Column Count:";
            this.lbResourceCount.Click += new System.EventHandler(this.lbResourceCount_Click);
            // 
            // rgrpColumnArrangement
            // 
            this.rgrpColumnArrangement.Location = new System.Drawing.Point(332, 10);
            this.rgrpColumnArrangement.Name = "rgrpColumnArrangement";
            this.rgrpColumnArrangement.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rgrpColumnArrangement.Properties.Appearance.Options.UseBackColor = true;
            this.rgrpColumnArrangement.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rgrpColumnArrangement.Properties.Columns = 5;
            this.rgrpColumnArrangement.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(DevExpress.XtraScheduler.Reporting.ColumnArrangementMode.Ascending, "Ascending"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(DevExpress.XtraScheduler.Reporting.ColumnArrangementMode.Descending, "Descending")});
            this.rgrpColumnArrangement.Size = new System.Drawing.Size(175, 24);
            this.rgrpColumnArrangement.TabIndex = 7;
            this.rgrpColumnArrangement.SelectedIndexChanged += new System.EventHandler(this.rgrpColumnMode_SelectedIndexChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(226, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(105, 13);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Column Arrangement:";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // PreviewControl
            // 
            this.Name = "PreviewControl";
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fPrintBarManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            this.pnlSettings.ResumeLayout(false);
            this.pnlSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinColumnCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgrpColumnArrangement.Properties)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		void spinColumnPerPage_EditValueChanged(object sender, EventArgs e) {
            this.visibleWeekDayColumnCount = Convert.ToInt32(spinColumnCount.Value);
			UpdateActiveReport();
		}
        void rgrpColumnMode_SelectedIndexChanged(object sender, EventArgs e) {
            this.columnArrangement = (ColumnArrangementMode)rgrpColumnArrangement.EditValue;
            UpdateActiveReport();
        }

        private void lbResourceCount_Click(object sender, EventArgs e) {

        }

        private void labelControl1_Click(object sender, EventArgs e) {

        }
	}
}

