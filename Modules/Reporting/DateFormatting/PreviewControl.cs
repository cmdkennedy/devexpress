using System;
using DevExpress.XtraScheduler.Reporting;
using DevExpress.XtraEditors;
using DevExpress.XtraScheduler.Services;
using DevExpress.XtraScheduler.Drawing;
using DevExpress.XtraScheduler.Services.Implementation;
using System.Globalization;

namespace DevExpress.XtraScheduler.Demos.Reporting.DateFormatting {

	public class PreviewControl : SingleReportPreviewControl {
        private System.ComponentModel.IContainer components = null;
        private GroupControl grHorzApt;
        private GroupControl grVertApt;
        private ComboBoxEdit cmbVertAptEnd;
        private ComboBoxEdit cmbVertAptStart;
        private LabelControl labelControl6;
        private ComboBoxEdit cmbHorzAptEnd;
        private LabelControl labelControl4;
        private ComboBoxEdit cmbHorzAptStart;
        private GroupControl groupControl1;
        private LabelControl labelControl7;
        private ComboBoxEdit cmbHeaderCaptions;
        private LabelControl labelControl3;
        private LabelControl labelControl2;
        private LabelControl labelControl1;


		public PreviewControl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			InitializeControlValues();            
		}

		public new Report Report { get { return (Report)base.Report; } }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		protected override XtraSchedulerReport CreateReportInstance() {
			return new Report();
		}
        protected override void InitAdapterTimeInterval() {
            StoragePrintAdapter.TimeInterval = new TimeInterval(DemoUtils.Date, TimeSpan.FromDays(21));
        }
		void InitializeControlValues() {
            PopulateFormatCombo(cmbHorzAptStart);
            PopulateFormatCombo(cmbHorzAptEnd);
            PopulateFormatCombo(cmbVertAptStart);
            PopulateFormatCombo(cmbVertAptEnd);

            PopulateFormatCombo(cmbHeaderCaptions);

		}
        void PopulateFormatCombo(ComboBoxEdit comboBox) {
            comboBox.Properties.Items.Clear();
            comboBox.Properties.Items.Add("Default");
            comboBox.Properties.Items.AddRange(DateTimeFormatInfo.CurrentInfo.GetAllDateTimePatterns());
            comboBox.SelectedIndex = 0;
        }
        protected override void UpdateReportProperties(XtraSchedulerReport report) {
			base.UpdateReportProperties(report);
            UpdateFormatServices();
		}
        public void UpdateFormatServices() {
            StoragePrintAdapter.RemoveService(typeof(IHeaderCaptionService));
            IHeaderCaptionService customHeaderCaptionService = new CustomHeaderCaptionService(cmbHeaderCaptions.Text);
            StoragePrintAdapter.AddService(typeof(IHeaderCaptionService), customHeaderCaptionService);
            
            StoragePrintAdapter.RemoveService(typeof(IAppointmentFormatStringService));
            CustomAppointmentFormatStringService customAptFormatService = new CustomAppointmentFormatStringService();
            customAptFormatService.HorizontalAppointmentStart = cmbHorzAptStart.Text;
            customAptFormatService.HorizontalAppointmentEnd = cmbHorzAptEnd.Text;
            customAptFormatService.VerticalAppointmentStart = cmbVertAptStart.Text;
            customAptFormatService.VerticalAppointmentEnd = cmbVertAptEnd.Text;
            StoragePrintAdapter.AddService(typeof(IAppointmentFormatStringService), customAptFormatService);
        }


		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.grHorzApt = new DevExpress.XtraEditors.GroupControl();
            this.cmbHorzAptEnd = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cmbHorzAptStart = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.grVertApt = new DevExpress.XtraEditors.GroupControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cmbVertAptEnd = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbVertAptStart = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cmbHeaderCaptions = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            this.pnlSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grHorzApt)).BeginInit();
            this.grHorzApt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbHorzAptEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbHorzAptStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grVertApt)).BeginInit();
            this.grVertApt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVertAptEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVertAptStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbHeaderCaptions.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // smplEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEdit.Location = new System.Drawing.Point(476, 97);
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.groupControl1);
            this.pnlSettings.Controls.Add(this.grVertApt);
            this.pnlSettings.Controls.Add(this.grHorzApt);
            this.pnlSettings.Size = new System.Drawing.Size(700, 141);
            this.pnlSettings.Controls.SetChildIndex(this.btnEdit, 0);
            this.pnlSettings.Controls.SetChildIndex(this.grHorzApt, 0);
            this.pnlSettings.Controls.SetChildIndex(this.grVertApt, 0);
            this.pnlSettings.Controls.SetChildIndex(this.groupControl1, 0);
            // 
            // panelSeparatorControl
            // 
            this.panelSeparatorControl.Location = new System.Drawing.Point(0, 141);
            // 
            // printControl
            // 
            this.printControl.Location = new System.Drawing.Point(0, 149);
            this.printControl.Size = new System.Drawing.Size(700, 247);
            // 
            // grHorzApt
            // 
            this.grHorzApt.Controls.Add(this.cmbHorzAptEnd);
            this.grHorzApt.Controls.Add(this.labelControl4);
            this.grHorzApt.Controls.Add(this.cmbHorzAptStart);
            this.grHorzApt.Controls.Add(this.labelControl1);
            this.grHorzApt.Location = new System.Drawing.Point(8, 8);
            this.grHorzApt.Name = "grHorzApt";
            this.grHorzApt.Size = new System.Drawing.Size(220, 125);
            this.grHorzApt.TabIndex = 54;
            this.grHorzApt.Text = "Horizontal Appointment";
            // 
            // cmbHorzAptEnd
            // 
            this.cmbHorzAptEnd.Location = new System.Drawing.Point(12, 93);
            this.cmbHorzAptEnd.Name = "cmbHorzAptEnd";
            this.cmbHorzAptEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "Default", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", null, null, true)});
            this.cmbHorzAptEnd.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbHorzAptEnd.Size = new System.Drawing.Size(196, 20);
            this.cmbHorzAptEnd.TabIndex = 53;
            this.cmbHorzAptEnd.SelectedIndexChanged += new System.EventHandler(this.cmbHorzAptStart_SelectedIndexChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(12, 74);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(85, 13);
            this.labelControl4.TabIndex = 52;
            this.labelControl4.Text = "End Date Format:";
            // 
            // cmbHorzAptStart
            // 
            this.cmbHorzAptStart.Location = new System.Drawing.Point(12, 48);
            this.cmbHorzAptStart.Name = "cmbHorzAptStart";
            this.cmbHorzAptStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "Default", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "", null, null, true)});
            this.cmbHorzAptStart.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbHorzAptStart.Size = new System.Drawing.Size(196, 20);
            this.cmbHorzAptStart.TabIndex = 51;
            this.cmbHorzAptStart.SelectedIndexChanged += new System.EventHandler(this.cmbHorzAptStart_SelectedIndexChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(91, 13);
            this.labelControl1.TabIndex = 50;
            this.labelControl1.Text = "Start Date Format:";
            // 
            // grVertApt
            // 
            this.grVertApt.Controls.Add(this.labelControl3);
            this.grVertApt.Controls.Add(this.labelControl2);
            this.grVertApt.Controls.Add(this.cmbVertAptEnd);
            this.grVertApt.Controls.Add(this.cmbVertAptStart);
            this.grVertApt.Controls.Add(this.labelControl6);
            this.grVertApt.Location = new System.Drawing.Point(234, 8);
            this.grVertApt.Name = "grVertApt";
            this.grVertApt.Size = new System.Drawing.Size(220, 125);
            this.grVertApt.TabIndex = 55;
            this.grVertApt.Text = "Vertical Appointment";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 74);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(85, 13);
            this.labelControl3.TabIndex = 55;
            this.labelControl3.Text = "End Date Format:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 29);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(91, 13);
            this.labelControl2.TabIndex = 54;
            this.labelControl2.Text = "Start Date Format:";
            // 
            // cmbVertAptEnd
            // 
            this.cmbVertAptEnd.Location = new System.Drawing.Point(12, 93);
            this.cmbVertAptEnd.Name = "cmbVertAptEnd";
            this.cmbVertAptEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "Default", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.cmbVertAptEnd.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbVertAptEnd.Size = new System.Drawing.Size(196, 20);
            this.cmbVertAptEnd.TabIndex = 53;
            this.cmbVertAptEnd.SelectedIndexChanged += new System.EventHandler(this.cmbHorzAptStart_SelectedIndexChanged);
            // 
            // cmbVertAptStart
            // 
            this.cmbVertAptStart.Location = new System.Drawing.Point(12, 48);
            this.cmbVertAptStart.Name = "cmbVertAptStart";
            this.cmbVertAptStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "Default", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", null, null, true)});
            this.cmbVertAptStart.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbVertAptStart.Size = new System.Drawing.Size(196, 20);
            this.cmbVertAptStart.TabIndex = 51;
            this.cmbVertAptStart.SelectedIndexChanged += new System.EventHandler(this.cmbHorzAptStart_SelectedIndexChanged);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(5, 24);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(0, 13);
            this.labelControl6.TabIndex = 50;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.cmbHeaderCaptions);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Location = new System.Drawing.Point(464, 8);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(220, 80);
            this.groupControl1.TabIndex = 56;
            this.groupControl1.Text = "Day Headers";
            // 
            // cmbHeaderCaptions
            // 
            this.cmbHeaderCaptions.Location = new System.Drawing.Point(12, 48);
            this.cmbHeaderCaptions.Name = "cmbHeaderCaptions";
            this.cmbHeaderCaptions.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "Default", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.cmbHeaderCaptions.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbHeaderCaptions.Size = new System.Drawing.Size(196, 20);
            this.cmbHeaderCaptions.TabIndex = 53;
            this.cmbHeaderCaptions.SelectedIndexChanged += new System.EventHandler(this.cmbHorzAptStart_SelectedIndexChanged);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(12, 29);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(38, 13);
            this.labelControl7.TabIndex = 50;
            this.labelControl7.Text = "Format:";
            // 
            // PreviewControl
            // 
            this.Name = "PreviewControl";
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storagePrintAdapter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            this.pnlSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grHorzApt)).EndInit();
            this.grHorzApt.ResumeLayout(false);
            this.grHorzApt.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbHorzAptEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbHorzAptStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grVertApt)).EndInit();
            this.grVertApt.ResumeLayout(false);
            this.grVertApt.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVertAptEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVertAptStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbHeaderCaptions.Properties)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

        private void cmbHorzAptStart_SelectedIndexChanged(object sender, EventArgs e) {
            UpdateActiveReport();
        }

	}

   public class CustomHeaderCaptionService : HeaderCaptionServiceWrapper {
       string format;
        public CustomHeaderCaptionService(string format) : base(new HeaderCaptionService()) {
            this.format = format;
        }
       protected virtual string CreateFormat(string format) {
           if (format == "Default")
               return string.Empty;
           return String.Format("{{0:{0}}}", format);
       }
       public override string GetDayColumnHeaderCaption(DayHeader header) {
            return CreateFormat(format);
        }
    }
    public class CustomAppointmentFormatStringService : AppointmentFormatStringServiceWrapper {
        string horizontalAppointmentStart;
        string horizontalAppointmentEnd;
        string verticalAppointmentStart;
        string verticalAppointmentEnd;
        
        public CustomAppointmentFormatStringService(): base(new AppointmentFormatStringService()) { 
        }
        public string HorizontalAppointmentStart { get { return horizontalAppointmentStart; } set { horizontalAppointmentStart = value; } }
        public string HorizontalAppointmentEnd { get { return horizontalAppointmentEnd; } set { horizontalAppointmentEnd = value; } }
        public string VerticalAppointmentStart { get { return verticalAppointmentStart; } set { verticalAppointmentStart = value; } }
        public string VerticalAppointmentEnd { get { return verticalAppointmentEnd; } set { verticalAppointmentEnd = value; } }

        protected virtual string CreateFormat(string format) {
            if (format == "Default")
                return string.Empty;
            return String.Format("{{0:{0}}} ", format);
        }
        public override string GetHorizontalAppointmentStartFormat(IAppointmentViewInfo aptViewInfo) {
            return CreateFormat(horizontalAppointmentStart);
        }
        public override string GetHorizontalAppointmentEndFormat(IAppointmentViewInfo aptViewInfo) {
            return CreateFormat(horizontalAppointmentEnd);
        }
        public override string GetVerticalAppointmentStartFormat(IAppointmentViewInfo aptViewInfo) {
            return CreateFormat(verticalAppointmentStart);
        }
        public override string GetVerticalAppointmentEndFormat(IAppointmentViewInfo aptViewInfo) {
            return CreateFormat(verticalAppointmentEnd);
        }
        public override string GetContinueItemStartFormat(IAppointmentViewInfo aptViewInfo) {
            return base.GetContinueItemStartFormat(aptViewInfo);
        }
        public override string GetContinueItemEndFormat(IAppointmentViewInfo aptViewInfo) {
            return base.GetContinueItemEndFormat(aptViewInfo);
        }
    }


}

