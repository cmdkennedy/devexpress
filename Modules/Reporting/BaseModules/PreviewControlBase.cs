using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UserDesigner;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraScheduler.Reporting;

namespace DevExpress.XtraScheduler.Demos.Reporting {
    
    /// <summary>
	/// Summary description for ModuleControl.
	/// </summary>
    /// 
    public class ReportPreviewControlBase : TutorialControl, IDemoSchedulerReport {

		public class DesignForm : DevExpress.XtraReports.UserDesigner.XRDesignFormEx {
			protected override void SaveLayout() { }
			protected override void RestoreLayout() { }
        }
        #region Fields
        private System.ComponentModel.IContainer components;
        private bool preventUpdate;
		protected PrintBarManager fPrintBarManager;
        protected DevExpress.XtraEditors.PanelControl pnlSettings;
        protected DevExpress.XtraEditors.PanelControl panelSeparatorControl;
        protected PrintControl printControl;
        protected DevExpress.XtraEditors.SimpleButton btnEdit;

        
        protected SchedulerStorage schedulerStorage;
        protected SchedulerStoragePrintAdapter storagePrintAdapter;
        #endregion		
		
        public ReportPreviewControlBase() {
			InitializeComponent();			
			this.fPrintBarManager = CreatePrintBarManager(printControl);
            FillReportSourceData();
            InitAdapterTimeInterval();
            CreateReports();
        }
        #region Properties
        protected bool PreventUpdate { get { return preventUpdate; } set { preventUpdate = value; } }
        public SchedulerStoragePrintAdapter StoragePrintAdapter { get { return this.storagePrintAdapter; } }
        public SchedulerStorage SchedulerStorage { get { return this.schedulerStorage; } }
        #endregion
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.panelSeparatorControl = new DevExpress.XtraEditors.PanelControl();
            this.pnlSettings = new DevExpress.XtraEditors.PanelControl();
            this.printControl = new DevExpress.XtraPrinting.Control.PrintControl();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.storagePrintAdapter = new DevExpress.XtraScheduler.Reporting.SchedulerStoragePrintAdapter();
            this.schedulerStorage = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).BeginInit();
            this.pnlSettings.SuspendLayout();
            this.SuspendLayout();

            // 
            // storagePrintAdapter
            // 
            this.storagePrintAdapter.SchedulerStorage = this.schedulerStorage;
            // 
            // panelSeparatorControl
            // 
            this.panelSeparatorControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelSeparatorControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSeparatorControl.Location = new System.Drawing.Point(0, 37);
            this.panelSeparatorControl.Name = "panelSeparatorControl";
            this.panelSeparatorControl.Size = new System.Drawing.Size(700, 8);
            this.panelSeparatorControl.TabIndex = 10;
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.btnEdit);
            this.pnlSettings.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSettings.Location = new System.Drawing.Point(0, 0);
            this.pnlSettings.Name = "pnlSettings";
            this.pnlSettings.Size = new System.Drawing.Size(700, 37);
            this.pnlSettings.TabIndex = 3;
            // 
            // printControl
            // 
            this.printControl.BackColor = System.Drawing.Color.Empty;
            this.printControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.printControl.ForeColor = System.Drawing.Color.Empty;
            this.printControl.IsMetric = false;
            this.printControl.Location = new System.Drawing.Point(0, 45);
            this.printControl.Name = "printControl";
            this.printControl.Size = new System.Drawing.Size(700, 351);
            this.printControl.TabIndex = 1;
            this.printControl.TabStop = false;
            this.printControl.TooltipFont = new System.Drawing.Font("Tahoma", 8.25F);
            this.printControl.VisibleChanged += new System.EventHandler(this.printControl_VisibleChanged);
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEdit.Location = new System.Drawing.Point(620, 8);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 24);
            this.btnEdit.TabIndex = 2;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // PreviewControlBase
            // 
            this.Controls.Add(this.printControl);
            this.Controls.Add(this.panelSeparatorControl);
            this.Controls.Add(this.pnlSettings);
            this.Name = "PreviewControlBase";
            this.Size = new System.Drawing.Size(700, 396);
            ((System.ComponentModel.ISupportInitialize)(this.panelSeparatorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSettings)).EndInit();
            this.pnlSettings.ResumeLayout(false);
            this.ResumeLayout(false);


        }
        #endregion        
        
        void InitializePrintingSystem(PrintingSystemBase printingSystem) {
            printingSystem.SetCommandVisibility(PrintingSystemCommand.ClosePreview, DevExpress.XtraPrinting.CommandVisibility.None);            
        }
        protected virtual void InitAdapterTimeInterval() {
            StoragePrintAdapter.TimeInterval = new TimeInterval(DemoUtils.Date, TimeSpan.FromDays(60));
        }               
		
		protected virtual void FillReportSourceData() {
            DemoUtils.FillStorageData(SchedulerStorage);
		}        
		protected virtual void UpdateReportProperties(XtraSchedulerReport report) {
            report.PrintColorSchema = DemoUtils.ReportPrintColorSchema;
            report.SchedulerAdapter = StoragePrintAdapter;
		}        
		protected PrintBarManager CreatePrintBarManager(PrintControl pc) {
			PrintBarManager printBarManager = new PrintBarManager();
			printBarManager.Form = printControl;
			printBarManager.Initialize(pc);
			printBarManager.MainMenu.Visible = false;
			printBarManager.AllowCustomization = false;
			return printBarManager;
		}
		protected void ShowDesignerForm(Form designForm, Form parentForm) {
            designForm.MinimumSize = parentForm.MinimumSize;
            if(parentForm.WindowState == FormWindowState.Normal)
			    designForm.Bounds = parentForm.Bounds;
            designForm.WindowState = parentForm.WindowState;
			parentForm.Visible = false;
			designForm.ShowDialog();
            parentForm.Visible = true;
		}		
        private void printControl_VisibleChanged(object sender, EventArgs e) {
            if (Visible && !preventUpdate) {
                UpdateActiveReport();                
            }
        }

        private void btnEdit_Click(object sender, EventArgs e) {
            CustomDesignForm designForm = new CustomDesignForm();
            XtraSchedulerReport report = GetActiveReport();
            if (report == null)
                return;
            
            designForm.OpenReport(report);
            PreventUpdate = true;
            try {
                ShowDesignerForm(designForm, this.FindForm());
            }
            finally {
                PreventUpdate = false;
            }
            designForm.Dispose();

            CreateReports();
            UpdateActiveReport();
        }

        void UpdatePrintingSystemDocument(XtraSchedulerReport report) {
            if (report == null || report.PrintingSystem.Document.IsCreating == true)
                return;
            Invalidate();
            Update();
            InitializePrintingSystem(report.PrintingSystem);
            printControl.PrintingSystem = report.PrintingSystem;
            report.CreateDocument(true);
        }	
        #region IDemoSchedulerReport Members
        public  void UpdateActiveReport() {
            XtraSchedulerReport report = GetActiveReport();
            if (report == null)
                return;
            UpdateReportProperties(report);
            UpdatePrintingSystemDocument(report);
        }    
        
        #endregion
        protected virtual void CreateReports() {
        }
        protected virtual XtraSchedulerReport GetActiveReport() {
            return null;
        }       
        
    }
}
