using System;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraScheduler.iCalendar;
using DevExpress.XtraScheduler.iCalendar.Components;
using DevExpress.XtraEditors;

namespace DevExpress.XtraScheduler.Demos {
	public partial class iCalendarImportModule : DevExpress.XtraScheduler.Demos.TutorialControl {
        string filePath;
		public iCalendarImportModule() {
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		protected bool IsCancelForRecurring { get { return (UsedAppointmentType)cbCancelingTypes.EditValue == UsedAppointmentType.Recurring; } }
		protected bool IsCancelForNonRecurring { get { return (UsedAppointmentType)cbCancelingTypes.EditValue == UsedAppointmentType.NonRecurring; } }

		void iCalendarImportModule_Load(object sender, EventArgs e) {
			DemoUtils.FillStorageResources(schedulerControl.Storage);
			cbCancelingTypes.EditValue = UsedAppointmentType.None;
			schedulerControl.Storage.EnableReminders = true;
		}

		void btnImport_Click(object sender, EventArgs e) {
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.Filter = "iCalendar files (*.ics)|*.ics";
			dialog.FilterIndex = 1;
			if(dialog.ShowDialog() != DialogResult.OK)
				return;
			BeforeImportActions();
			using(Stream stream = dialog.OpenFile()) {
                this.filePath = dialog.FileName;
			    ImportAppointments(stream);
			}
		    AfterImportActions();
		}

		void ImportAppointments(Stream stream) {
			if(stream == null)
				return;
			iCalendarImporter importer = new iCalendarImporter(schedulerStorage);
			importer.CalendarStructureCreated += new iCalendarStructureCreatedEventHandler(importer_CalendarStructureCreated);
			importer.AppointmentImporting += new AppointmentImportingEventHandler(importer_AppointmentImporting);
            importer.OnException += new ExchangeExceptionEventHandler(importer_OnException);
			importer.Import(stream);
		}

        void importer_OnException(object sender, ExchangeExceptionEventArgs e) {
            if (e.OriginalException is iCalendarInvalidFileFormatException) {
                string message = String.Format(@"The file ""{0}"" is not a valid Internet Calendar file", Path.GetFileName(this.filePath));
                XtraMessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            iCalendarParseExceptionEventArgs args = e as iCalendarParseExceptionEventArgs;
            if (args != null) {
                XtraMessageBox.Show(String.Format("Can't parse line '{1}' at {0} index", args.LineIndex, args.LineText));
                iCalendarImporter importer = (iCalendarImporter)sender;
                importer.Terminate();
            } 
            e.Handled = true;
        }
		void importer_AppointmentImporting(object sender, AppointmentImportingEventArgs e) {
			ProgressBarControl1.Position++;
			ProgressBarControl1.Update();
			bool cancel = e.Appointment.IsRecurring ? IsCancelForRecurring : IsCancelForNonRecurring;
			if(cancel) {
			    e.Cancel = true;
			}
		}
		void importer_CalendarStructureCreated(object sender, iCalendarStructureCreatedEventArgs e) {
			iCalendarImporter importer = (iCalendarImporter)sender;
			ProgressBarControl1.Position = 0;
			ProgressBarControl1.Properties.Maximum = importer.SourceObjectCount;
		}
		
		void BeforeImportActions() {
			if(chkClearBeforImport.Checked)
				schedulerStorage.Appointments.Clear();
		}
		void AfterImportActions() {
			ProgressBarControl1.Properties.Maximum = 100;
			ProgressBarControl1.Position = 0;
		}
	}
}
