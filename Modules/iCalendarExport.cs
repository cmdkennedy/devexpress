using System;
using System.Windows.Forms;
using DevExpress.XtraScheduler.iCalendar;
using System.IO;

namespace DevExpress.XtraScheduler.Demos {
	public partial class iCalendarExportModule : DevExpress.XtraScheduler.Demos.TutorialControl {
		public iCalendarExportModule() {
			InitializeComponent();
		}

		protected bool IsCancelForRecurring { get { return (UsedAppointmentType)cbCancelingTypes.EditValue == UsedAppointmentType.Recurring; } }
		protected bool IsCancelForNonRecurring { get { return (UsedAppointmentType)cbCancelingTypes.EditValue == UsedAppointmentType.NonRecurring; } }

		void iCalendarExportModule_Load(object sender, EventArgs e) {
			DemoUtils.FillData(schedulerControl);
			cbCancelingTypes.EditValue = UsedAppointmentType.None;
			schedulerControl.Storage.EnableReminders = true;
		}
		private void btnExport_Click(object sender, EventArgs e) {
			SaveFileDialog fileDialog = new SaveFileDialog();
			fileDialog.Filter = "iCalendar files (*.ics)|*.ics";
			fileDialog.FilterIndex = 1;
			if(fileDialog.ShowDialog() != DialogResult.OK)
				return;
			try {
				using(Stream stream = fileDialog.OpenFile())
					ExportAppointments(stream);
			}
			catch {
				MessageBox.Show("Error: could not export appointments");
			}
			finally {
				AfterExportActions();
			}
		}
		void AfterExportActions() {
			ProgressBarControl1.Properties.Maximum = 100;
			ProgressBarControl1.Position = 0;
		}
		void ExportAppointments(Stream stream) {
			if(stream == null)
				return;
			iCalendarExporter exporter = new iCalendarExporter(schedulerStorage);

			ProgressBarControl1.Properties.Maximum = exporter.SourceObjectCount;
			ProgressBarControl1.Position = 0;		

			exporter.ProductIdentifier = "-//Developer Express Inc.//XtraScheduler iCalendarExportDemo//EN";
			exporter.AppointmentExporting += new AppointmentExportingEventHandler(exporter_AppointmentExporting);
			exporter.Export(stream);
		}
		void exporter_AppointmentExporting(object sender, AppointmentExportingEventArgs e) {
			ProgressBarControl1.Position++;
			ProgressBarControl1.Update();
			bool cancel = e.Appointment.IsRecurring ? IsCancelForRecurring : IsCancelForNonRecurring;
			if(cancel) {
				e.Cancel = true;
			}
		}
	}
}
