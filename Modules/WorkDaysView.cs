using System;
using DevExpress.XtraScheduler;

namespace DevExpress.XtraScheduler.Demos {
	public partial class WorkWeekViewModule : DevExpress.XtraScheduler.Demos.TutorialControl {
		public WorkWeekViewModule() {
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			PrepareViews();
			weekDaysCheckEdit1.WeekDays = WeekDays.WorkDays;
		}

		public override SchedulerControl PrintingSchedulerControl { get { return schedulerControl; } }

		void PrepareViews() {
			schedulerControl.DayView.Enabled = false;
			schedulerControl.WeekView.Enabled = false;
			schedulerControl.MonthView.Enabled = false;
			schedulerControl.TimelineView.Enabled = false;
		}
		private void WorkWeekViewModule_Load(object sender, System.EventArgs e) {
			DemoUtils.FillData(schedulerControl);
		}

		void weekDaysCheckEdit1_WeekDaysChanged(object sender, System.EventArgs e) {
			WorkDaysCollection workDays = schedulerControl.WorkDays;
			workDays.BeginUpdate();
			workDays.Clear();
			if (weekDaysCheckEdit1.WeekDays != (WeekDays)0)
				workDays.Add(new WeekDaysWorkDay(weekDaysCheckEdit1.WeekDays));
			workDays.EndUpdate();
		}

		private void schedulerControl_PopupMenuShowing(object sender, DevExpress.XtraScheduler.PopupMenuShowingEventArgs e) {
			if (e.Menu.Id == SchedulerMenuItemId.DefaultMenu || e.Menu.Id == SchedulerMenuItemId.RulerMenu) {
				e.Menu.RemoveMenuItem(SchedulerMenuItemId.SwitchViewMenu);
			}
		}

		private void WorkWeekViewModule_VisibleChanged(object sender, System.EventArgs e) {
			schedulerControl.ActiveViewType = SchedulerViewType.WorkWeek;
		}
	}
}
