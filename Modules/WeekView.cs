using System;

namespace DevExpress.XtraScheduler.Demos {
	public partial class WeekViewModule : DevExpress.XtraScheduler.Demos.TutorialControl {

		public WeekViewModule() {
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			PrepareViews();
		}

		public override SchedulerControl PrintingSchedulerControl { get { return schedulerControl; } }
		protected AppointmentDisplayOptions DisplayOptions { get { return schedulerControl.WeekView.AppointmentDisplayOptions; } }

		void PrepareViews() {
			schedulerControl.DayView.Enabled = true;
			schedulerControl.WorkWeekView.Enabled = false;
			schedulerControl.MonthView.Enabled = false;
			schedulerControl.TimelineView.Enabled = false;
		}

		private void WeekViewModule_Load(object sender, System.EventArgs e) {
			DemoUtils.FillData(schedulerControl);
			UpdateControls();
		}
		void UpdateControls() {
			AppointmentDisplayOptions options = DisplayOptions;
			chkStartTimeVisibility.CheckState = DemoUtils.TimeVisibilityToCheckState(options.StartTimeVisibility);
			chkEndTimeVisibility.CheckState = DemoUtils.TimeVisibilityToCheckState(options.EndTimeVisibility);

			cbTimeDisplayType.EditValue = options.TimeDisplayType;
			cbStatus.EditValue = options.StatusDisplayType;
		}
		private void WeekViewModule_VisibleChanged(object sender, System.EventArgs e) {
			schedulerControl.ActiveViewType = SchedulerViewType.Week;
		}

		private void chkStartTimeVisibility_CheckStateChanged(object sender, EventArgs e) {
			DisplayOptions.StartTimeVisibility = DemoUtils.CheckStateToTimeVisibility(chkStartTimeVisibility.CheckState);
		}

		private void chkEndTimeVisibility_CheckStateChanged(object sender, EventArgs e) {
			DisplayOptions.EndTimeVisibility = DemoUtils.CheckStateToTimeVisibility(chkEndTimeVisibility.CheckState);
		}

		private void cbTimeDisplayType_SelectedIndexChanged(object sender, EventArgs e) {
			DisplayOptions.TimeDisplayType = (AppointmentTimeDisplayType)cbTimeDisplayType.EditValue;
		}

		private void cbStatus_SelectedIndexChanged(object sender, EventArgs e) {
			DisplayOptions.StatusDisplayType = (AppointmentStatusDisplayType)cbStatus.EditValue;
		}
	}
}
