using System;
using DevExpress.XtraScheduler;

namespace DevExpress.XtraScheduler.Demos {
	public partial class DayViewModule : DevExpress.XtraScheduler.Demos.TutorialControl {
		public DayViewModule() {
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			PrepareViews();

			// TODO: Add any initialization after the InitializeComponent call
		}

		public override SchedulerControl PrintingSchedulerControl { get { return schedulerControl; } }
		protected DayViewAppointmentDisplayOptions DisplayOptions { get { return (DayViewAppointmentDisplayOptions)schedulerControl.DayView.AppointmentDisplayOptions; } }

		void PrepareViews() {
			schedulerControl.WeekView.Enabled = false;
			schedulerControl.WorkWeekView.Enabled = false;
			schedulerControl.MonthView.Enabled = false;
			schedulerControl.TimelineView.Enabled = false;
		}
		private void DayViewModule_Load(object sender, System.EventArgs e) {
			DemoUtils.FillData(schedulerControl);
			UpdateControls();
		}

		private void UpdateControls() {
			chkShowWorkTimeOnly.Checked = schedulerControl.DayView.ShowWorkTimeOnly;
			chkShowAllDayArea.Checked = schedulerControl.DayView.ShowAllDayArea;
			chkShowDayHeaders.Checked = schedulerControl.DayView.ShowDayHeaders;
			chkAppointmentShadows.Checked = DisplayOptions.ShowShadows;

			spinDaysCount.EditValue = schedulerControl.DayView.DayCount;

			cbAllDayStatus.EditValue = DisplayOptions.AllDayAppointmentsStatusDisplayType;
			cbStatus.EditValue = DisplayOptions.StatusDisplayType;
			cbSnapToCellsMode.EditValue = DisplayOptions.SnapToCellsMode;
		}
		private void chkShowWorkTimeOnly_CheckedChanged(object sender, System.EventArgs e) {
			schedulerControl.DayView.ShowWorkTimeOnly = chkShowWorkTimeOnly.Checked;
		}

		private void chkShowAllDayArea_CheckedChanged(object sender, System.EventArgs e) {
			schedulerControl.DayView.ShowAllDayArea = chkShowAllDayArea.Checked;
		}

		private void chkShowDayHeaders_CheckedChanged(object sender, System.EventArgs e) {
			schedulerControl.DayView.ShowDayHeaders = chkShowDayHeaders.Checked;
		}

		private void chkAppointmentShadows_CheckedChanged(object sender, System.EventArgs e) {
			schedulerControl.DayView.AppointmentDisplayOptions.ShowShadows = chkAppointmentShadows.Checked;
		}

		private void spinDaysCount_EditValueChanged(object sender, System.EventArgs e) {
			schedulerControl.DayView.DayCount = Convert.ToInt32(spinDaysCount.EditValue);
		}

		private void schedulerControl_PopupMenuShowing(object sender, DevExpress.XtraScheduler.PopupMenuShowingEventArgs e) {
			if (e.Menu.Id == SchedulerMenuItemId.DefaultMenu || e.Menu.Id == SchedulerMenuItemId.RulerMenu) {
				e.Menu.RemoveMenuItem(SchedulerMenuItemId.SwitchViewMenu);
			}
		}
		private void DayViewModule_VisibleChanged(object sender, System.EventArgs e) {
			schedulerControl.ActiveViewType = SchedulerViewType.Day;
		}

		private void cbStatus_SelectedIndexChanged(object sender, EventArgs e) {
			DisplayOptions.StatusDisplayType = (AppointmentStatusDisplayType)cbStatus.EditValue;
		}

		private void cbAllDayStatus_SelectedIndexChanged(object sender, EventArgs e) {
			DisplayOptions.AllDayAppointmentsStatusDisplayType = (AppointmentStatusDisplayType)cbAllDayStatus.EditValue;
		}

		private void cbSnapToCellsMode_SelectedIndexChanged(object sender, EventArgs e) {
			DisplayOptions.SnapToCellsMode = (AppointmentSnapToCellsMode)cbSnapToCellsMode.EditValue;
		}
	}
}

