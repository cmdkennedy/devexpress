using System;

namespace DevExpress.XtraScheduler.Demos {
	public partial class TimelineViewModule : DevExpress.XtraScheduler.Demos.TutorialControl {

		public TimelineViewModule() {
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			PrepareViews();
		}
		public override SchedulerControl PrintingSchedulerControl { get { return schedulerControl; } }
		protected AppointmentDisplayOptions DisplayOptions { get { return schedulerControl.TimelineView.AppointmentDisplayOptions; } }

		void PrepareViews() {
			schedulerControl.DayView.Enabled = true;
			schedulerControl.WeekView.Enabled = false;
			schedulerControl.WorkWeekView.Enabled = false;
			schedulerControl.MonthView.Enabled = false;
		}

		private void TimelineViewModule_Load(object sender, System.EventArgs e) {
			DemoUtils.FillData(schedulerControl);
			UpdateControls();
		}
		private void TimelineViewModule_VisibleChanged(object sender, System.EventArgs e) {
			schedulerControl.ActiveViewType = SchedulerViewType.Timeline;
		}
		void UpdateControls() {
			AppointmentDisplayOptions options = DisplayOptions;
			chkAutoHeight.Checked = options.AppointmentAutoHeight;
			spinHeight.Value = options.AppointmentHeight;
			cbSnapToCellsMode.EditValue = options.SnapToCellsMode;
			cbStatus.EditValue = options.StatusDisplayType;
			chkSelectionBarVisible.Checked = schedulerControl.TimelineView.SelectionBar.Visible;
			spinSelectionBarHeight.Value = schedulerControl.TimelineView.SelectionBar.Height;
            UpdateTimeScaleWidthTrackBar();
		}

        private void UpdateTimeScaleWidthTrackBar() {
            schedulerControl.TimelineView.GetBaseTimeScale().Width = trckScaleWidth.Value;
            trckScaleWidth.Enabled = schedulerControl.TimelineView.Scales.Count > 0;
        }
        private void chkSelectionBarVisible_CheckedChanged(object sender, EventArgs e) {
			schedulerControl.TimelineView.SelectionBar.Visible = chkSelectionBarVisible.Checked;
		}
		private void spinSelectionBarHeight_EditValueChanged(object sender, EventArgs e) {
			schedulerControl.TimelineView.SelectionBar.Height = Convert.ToInt32(spinSelectionBarHeight.Value);
		}
		private void btnEditScales_Click(object sender, EventArgs e) {
			EditTimeScales(schedulerControl.TimelineView.Scales);
            UpdateTimeScaleWidthTrackBar();
		}
		void EditTimeScales(TimeScaleCollection scales) {
			TimeScalesEditForm frm = new TimeScalesEditForm(scales, this.FindForm());
			frm.ShowDialog();
		}
		private void chkAutoHeight_CheckedChanged(object sender, EventArgs e) {
			DisplayOptions.AppointmentAutoHeight = chkAutoHeight.Checked;
		}

		private void spinHeight_EditValueChanged(object sender, EventArgs e) {
			DisplayOptions.AppointmentHeight = Convert.ToInt32(spinHeight.Value);
		}

		private void cbStatus_EditValueChanged(object sender, EventArgs e) {
			DisplayOptions.StatusDisplayType = (AppointmentStatusDisplayType)cbStatus.EditValue;
		}

		private void cbSnapToCellsMode_EditValueChanged(object sender, EventArgs e) {
			DisplayOptions.SnapToCellsMode = (AppointmentSnapToCellsMode)cbSnapToCellsMode.EditValue;
		}

        private void trckScaleWidth_EditValueChanged(object sender, EventArgs e) {
            schedulerControl.TimelineView.GetBaseTimeScale().Width = trckScaleWidth.Value;
        }
	}
}
