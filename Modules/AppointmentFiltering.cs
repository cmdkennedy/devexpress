using System;
using System.Drawing;
using DevExpress.Utils;
using DevExpress.Utils.Controls;
using DevExpress.XtraScheduler.Drawing;

namespace DevExpress.XtraScheduler.Demos {
    public partial class AppointmentFilteringModule : DevExpress.XtraScheduler.Demos.TutorialControl {
        ImageCollection sportsImages;
        ImageCollection channelsImages;

        public AppointmentFilteringModule() {
            this.sportsImages = ImageHelper.CreateImageCollectionFromResources("DevExpress.XtraScheduler.Demos.Images.sports.png", System.Reflection.Assembly.GetExecutingAssembly(), new Size(16, 16));
            this.channelsImages = ImageHelper.CreateImageCollectionFromResources("DevExpress.XtraScheduler.Demos.Images.channels.png", System.Reflection.Assembly.GetExecutingAssembly(), new Size(60, 40));

            InitializeComponent();

        }
        public override SchedulerControl PrintingSchedulerControl { get { return schedulerControl; } }

        private void ResourceSharingModule_Load(object sender, System.EventArgs e) {
            schedulerControl.GroupType = SchedulerGroupType.Resource;

            FillFilterComboBox();
            AddSportChanels();
            FillData();
        }
        void FillData() {
            schedulerStorage.EnableReminders = false;
            this.schedulerStorage.Appointments.Mappings.End = "Finish";
            this.schedulerStorage.Appointments.Mappings.Label = "SportID";
            this.schedulerStorage.Appointments.Mappings.ResourceId = "ResourceID";
            this.schedulerStorage.Appointments.Mappings.Start = "Start";
            this.schedulerStorage.Appointments.Mappings.Subject = "Caption";
            this.schedulerStorage.Appointments.Mappings.AllDay = "AllDay";

            schedulerStorage.Appointments.DataSource = DemoUtils.GetSportEventsData();
            schedulerControl.Start = new DateTime(2014, 09, 15, 9, 0, 0);
        }
        void AddSportChanels() {
            schedulerStorage.Resources.BeginUpdate();
            AddResource(0, "SPORT TV 1");
            AddResource(1, "SPORT TV 2");
            AddResource(2, "SPORT TV 3");
            AddResource(3, "SPORT TV 4");
            AddResource(4, "TV 5");
            AddResource(5, "TV 6");
            AddResource(6, "TV 7");
            AddResource(7, "TV 8");
            schedulerStorage.Resources.EndUpdate();
        }
        void AddResource(int index, string caption) {
            Resource r = schedulerStorage.CreateResource(index.ToString());
            r.Caption = caption;
            r.Image = this.channelsImages.Images[index];
            r.Color = schedulerControl.ResourceColorSchemas.GetSchema(index).CellLight;
            schedulerStorage.Resources.Add(r);
        }
        void FillFilterComboBox() {
            this.imcbSports.Properties.SmallImages = this.sportsImages;
            this.imcbSports.Properties.LargeImages = this.sportsImages;
            this.imcbSports.Properties.Items.Clear();
            this.imcbSports.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem("All sports", -1, -1));
            for (int i = 0; i < schedulerStorage.Appointments.Labels.Count; i++) {
                AppointmentLabel lab = schedulerStorage.Appointments.Labels[i];
                this.imcbSports.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ImageComboBoxItem(lab.DisplayName, i, i));
            }
            this.imcbSports.SelectedIndex = 0;
        }
        private void cbView_SelectedIndexChanged(object sender, System.EventArgs e) {
            schedulerControl.BeginUpdate();
            try {
                AdjustResourceHeaders();
            }
            finally {
                schedulerControl.EndUpdate();
            }
        }
        private void schedulerControl_ActiveViewChanged(object sender, System.EventArgs e) {
        }
        private void cbGrouping_SelectedIndexChanged(object sender, EventArgs e) {
            schedulerControl.BeginUpdate();
            try {
                AdjustResourceHeaders();
            }
            finally {
                schedulerControl.EndUpdate();
            }
        }
        void AdjustResourceHeaders() {
            int height = 0;
            SchedulerGroupType groupType = schedulerControl.GroupType;
            if ((schedulerControl.ActiveView is WeekView && groupType == SchedulerGroupType.Date) || (schedulerControl.ActiveView is TimelineView && groupType != SchedulerGroupType.None))
                height = 90;

            schedulerControl.OptionsView.ResourceHeaders.Height = height;
        }
        private void schedulerStorage_FilterAppointment(object sender, PersistentObjectCancelEventArgs e) {
            int sportId = Convert.ToInt32(this.imcbSports.EditValue);
            if (sportId < 0)
                return;

            Appointment apt = (Appointment)e.Object;
            e.Cancel = apt.LabelId != sportId;
        }

        private void imcbSports_SelectedIndexChanged(object sender, EventArgs e) {
            schedulerStorage.RefreshData();
        }

        private void schedulerControl_InitAppointmentImages(object sender, AppointmentImagesEventArgs e) {
            AppointmentImageInfo info = new AppointmentImageInfo();
            info.Image = this.sportsImages.Images[e.Appointment.LabelId];
            e.ImageInfoList.Add(info);
        }
    }
}
