using System;
using DevExpress.XtraScheduler;

namespace DevExpress.XtraScheduler.Demos {
    public partial class FullWeekViewModule : DevExpress.XtraScheduler.Demos.TutorialControl {
        public FullWeekViewModule() {
            // This call is required by the Windows Form Designer.
            InitializeComponent();

            PrepareViews();
        }

        public override SchedulerControl PrintingSchedulerControl { get { return schedulerControl1; } }

        void PrepareViews() {
            this.schedulerControl1.FullWeekView.Enabled = true;
            this.schedulerControl1.DayView.Enabled = false;
            this.schedulerControl1.WeekView.Enabled = false;
            this.schedulerControl1.MonthView.Enabled = false;
            this.schedulerControl1.WorkWeekView.Enabled = false;
            this.schedulerControl1.TimelineView.Enabled = false;
            this.schedulerControl1.GanttView.Enabled = false;
        }
        private void FullWeekViewModule_Load(object sender, System.EventArgs e) {
            DemoUtils.FillData(this.schedulerControl1);
        }
    }
}
