namespace DevExpress.XtraScheduler.Demos.Forms {
	partial class UserDefinedFilterAppointmentForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.lblPrice = new System.Windows.Forms.Label();
			this.edtPrice = new DevExpress.XtraEditors.CalcEdit();
			((System.ComponentModel.ISupportInitialize)(this.chkAllDay.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.edtStartDate.Properties.VistaTimeProperties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.edtStartDate.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.edtEndDate.Properties.VistaTimeProperties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.edtEndDate.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.edtStartTime.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.edtEndTime.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.edtLabel.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.edtShowTimeAs.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tbSubject.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.edtResource.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.edtResources.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkReminder.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tbDescription.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cbReminder.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tbLocation.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.edtPrice.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// chkAllDay
			// 
			// 
			// edtLabel
			// 
			// 
			// edtShowTimeAs
			// 
			// 
			// tbSubject
			// 
			// 
			// edtResource
			// 
			// 
			// edtResources
			// 
			// 
			// chkReminder
			// 
			// 
			// tbDescription
			// 
			this.tbDescription.Location = new System.Drawing.Point(16, 198);
			this.tbDescription.Size = new System.Drawing.Size(496, 144);
			this.tbDescription.TabIndex = 15;
			// 
			// cbReminder
			// 
			// 
			// tbLocation
			// 
			// 
			// lblPrice
			// 
			this.lblPrice.AutoSize = true;
			this.lblPrice.Location = new System.Drawing.Point(16, 168);
			this.lblPrice.Name = "lblPrice";
			this.lblPrice.Size = new System.Drawing.Size(34, 13);
			this.lblPrice.TabIndex = 37;
			this.lblPrice.Text = "&Price:";
			// 
			// edtPrice
			// 
			this.edtPrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.edtPrice.Location = new System.Drawing.Point(96, 165);
			this.edtPrice.Name = "edtPrice";
			this.edtPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.edtPrice.Size = new System.Drawing.Size(216, 19);
			this.edtPrice.TabIndex = 14;
			this.edtPrice.EditValueChanged += new System.EventHandler(this.edtPrice_EditValueChanged);
			// 
			// UserDefinedFilterAppointmentForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(528, 382);
			this.Controls.Add(this.edtPrice);
			this.Controls.Add(this.lblPrice);
			this.Name = "UserDefinedFilterAppointmentForm";
			this.Controls.SetChildIndex(this.tbDescription, 0);
			this.Controls.SetChildIndex(this.lblPrice, 0);
			this.Controls.SetChildIndex(this.edtPrice, 0);
			this.Controls.SetChildIndex(this.edtResources, 0);
			this.Controls.SetChildIndex(this.edtShowTimeAs, 0);
			this.Controls.SetChildIndex(this.edtEndTime, 0);
			this.Controls.SetChildIndex(this.edtEndDate, 0);
			this.Controls.SetChildIndex(this.btnRecurrence, 0);
			this.Controls.SetChildIndex(this.btnDelete, 0);
			this.Controls.SetChildIndex(this.btnCancel, 0);
			this.Controls.SetChildIndex(this.lblShowTimeAs, 0);
			this.Controls.SetChildIndex(this.lblEndTime, 0);
			this.Controls.SetChildIndex(this.lblLabel, 0);
			this.Controls.SetChildIndex(this.tbLocation, 0);
			this.Controls.SetChildIndex(this.lblSubject, 0);
			this.Controls.SetChildIndex(this.lblLocation, 0);
			this.Controls.SetChildIndex(this.tbSubject, 0);
			this.Controls.SetChildIndex(this.lblStartTime, 0);
			this.Controls.SetChildIndex(this.chkAllDay, 0);
			this.Controls.SetChildIndex(this.btnOk, 0);
			this.Controls.SetChildIndex(this.edtStartDate, 0);
			this.Controls.SetChildIndex(this.edtStartTime, 0);
			this.Controls.SetChildIndex(this.edtLabel, 0);
			this.Controls.SetChildIndex(this.chkReminder, 0);
			this.Controls.SetChildIndex(this.cbReminder, 0);
			this.Controls.SetChildIndex(this.lblResource, 0);
			this.Controls.SetChildIndex(this.edtResource, 0);
			((System.ComponentModel.ISupportInitialize)(this.chkAllDay.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.edtStartDate.Properties.VistaTimeProperties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.edtStartDate.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.edtEndDate.Properties.VistaTimeProperties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.edtEndDate.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.edtStartTime.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.edtEndTime.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.edtLabel.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.edtShowTimeAs.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tbSubject.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.edtResource.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.edtResources.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkReminder.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tbDescription.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cbReminder.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tbLocation.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.edtPrice.Properties)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblPrice;
		private DevExpress.XtraEditors.CalcEdit edtPrice;
	}
}
