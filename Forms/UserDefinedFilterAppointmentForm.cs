using System;
using DevExpress.XtraScheduler.UI;

namespace DevExpress.XtraScheduler.Demos.Forms {
	public partial class UserDefinedFilterAppointmentForm : AppointmentForm {
		public UserDefinedFilterAppointmentForm() {
			InitializeComponent();
		}
		public UserDefinedFilterAppointmentForm(SchedulerControl control, Appointment apt, bool openRecurrenceForm) : base(control, apt, openRecurrenceForm) {
			InitializeComponent();
			UpdateCustomFieldsControls();
		}

		protected internal new UserDefinedFilterAppointmentFormController Controller { get { return (UserDefinedFilterAppointmentFormController)base.Controller; } }

		protected override AppointmentFormController CreateController(SchedulerControl control, Appointment apt) {
			return new UserDefinedFilterAppointmentFormController(control, apt);
		}

		protected override void UpdateCustomFieldsControls() {
			base.UpdateCustomFieldsControls();
			if (edtPrice != null)
				edtPrice.EditValue = Controller.Price;
		}

		private void edtPrice_EditValueChanged(object sender, EventArgs e) {
			Controller.Price = Convert.ToDecimal(edtPrice.EditValue);
		}
	}

	public class UserDefinedFilterAppointmentFormController : AppointmentFormController {
		public UserDefinedFilterAppointmentFormController(SchedulerControl control, Appointment apt)
			: base(control, apt) {
		}

		public Decimal Price {
			get {
				object value = EditedAppointmentCopy.CustomFields["CustomFieldPrice"];
				try {
					return Convert.ToDecimal(value);
				}
				catch {
					return 0;
				}
			}
			set { EditedAppointmentCopy.CustomFields["CustomFieldPrice"] = value; }
		}

		protected override void ApplyCustomFieldsValues() {
			base.ApplyCustomFieldsValues();
			SourceAppointment.CustomFields["CustomFieldPrice"] = Price;
		}
	}
}
