using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace DevExpress.XtraScheduler.Demos {
    static class Program {
        [STAThread]
        static void Main() {
            DevExpress.UserSkins.BonusSkins.Register();
            Application.Run(new frmMain());
        }       
    }    
}
